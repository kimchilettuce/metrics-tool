Option Compare Database

Dim defaultFirstFix_days As Integer
Dim boolECall As Boolean


'***************************************************************************
' Purpose: Used for when the form loads, here we want to update the values
'           of the control elements and set its focus to true.
' Kinzey
'***************************************************************************
Private Sub Form_Load()
    Call getFromTblSettings
    
    Form_frmQryTDB_Settings.SetFocus
End Sub


'***************************************************************************
' Purpose: Used to lookup the values in tblSettings and display them
'           on the respective control elements
' Kinzey
'***************************************************************************
Private Sub getFromTblSettings()
    ' Read from tblSettings and set the label to show the default value for day diff
    
    ' TW 140322: Added capabaility for txtInputNormalTimeStart, txtInputNormalTimeend, and txtOutlierMins
    txtInputNormalTimeStart.Value = getSettings("Call Back: Normal Arrival Time")
    txtInputNormalTimeEnd.Value = getSettings("Call Back: Normal Leave Time")
    txtOutlierMins.Value = getSettings("Call Back: On-site Outlier (mins)")
       
    txtInputFirstFix_days.Value = getSettings("FTF Day Diff")
    optGrpECalls.Value = getSettings("FTF Include E Calls")

    optGrpMaint.Value = getSettings("Maint Stats Use Notif Dates")
    optGrpReq.Value = getSettings("Req Stats Use Notif Dates")
    optGrpOrders.Value = getSettings("Orders Stats Use Notif Dates")
End Sub


' --------------------------- Button Clicks -----------------------------------------------


'***************************************************************************
' Purpose: Adding a button for restoring the settings to their defaults
' Kinzey
'***************************************************************************
Private Sub btnDefaults_Click()
    ' this will invoke the calls back to tblSettings which will contain the default values
    Call resetSettingDefaultsRunSQL
    Call getFromTblSettings
End Sub


'***************************************************************************
' Purpose: For when user clicks back to go back to frmQryTDB_Result. Does not
'           load settings.
' Kinzey
'***************************************************************************
Private Sub btnBack_Click()
    DoCmd.Close acForm, "frmQryTDB_Settings", acSaveNo
End Sub


'***************************************************************************
' Purpose: For when user clicks back to go back to frmQryTDB_Result and to load
'           the settings too.
' Kinzey
'***************************************************************************
Private Sub btnLoad_Click()
    
    ' TW 140322: Added capabaility for txtInputNormalTimeStart, txtInputNormalTimeend, and txtOutlierMins
    Call updateSettings("Call Back: Normal Arrival Time", txtInputNormalTimeStart.Value)
    Call updateSettings("Call Back: Normal Leave Time", txtInputNormalTimeEnd.Value)
    Call updateSettings("Call Back: On-site Outlier (mins)", txtOutlierMins.Value)
    
    Call updateSettings("FTF Day Diff", txtInputFirstFix_days.Value)
    Call updateSettings("FTF Include E Calls", optGrpECalls.Value)

    Call updateSettings("Maint Stats Use Notif Dates", optGrpMaint.Value)
    Call updateSettings("Req Stats Use Notif Dates", optGrpReq.Value)
    Call updateSettings("Orders Stats Use Notif Dates", optGrpOrders.Value)
    
    Call Form_frmQryTDB_Result.Update_Graph(Form_frmQryTDB_Result.graphcriteria, Form_frmQryTDB_Result.graphorder)
    
    DoCmd.Close acForm, "frmQryTDB_Settings", acSaveNo
End Sub


'***************************************************************************
' Purpose: As the user types the settings for days for first fix, we only
'       want to accept backspace or numbers
' Kinzey
'***************************************************************************
Private Sub txtInputFirstFix_days_KeyPress(KeyAscii As Integer)
   ' By setting KeyAscii = 0, the value is flushed out and not printed.

    ' if it is a backspace, let it pass through
    If KeyAscii = 8 Then
        Exit Sub
    End If
    
    ' there is an overflow issue when integer is too long
    '       hence we just restrict the max length to be 4
    '       Note that we still want to allow
    If Len(txtInputFirstFix_days.Text) = 4 Then
        KeyAscii = 0
        Exit Sub
    End If

    ' If the number is non-numeric, then block it.
    If KeyAscii < 48 Or KeyAscii > 57 Then
        KeyAscii = 0
        Exit Sub
    End If
End Sub


' User pitfall to consider:
'       maybe will at one stage need to do user error handling. What if they click on the mainform
'       and this setting form goes behind it and "disappears".


