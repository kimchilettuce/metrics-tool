Option Explicit
Option Compare Database

Dim StoragePath As String

Sub globalInit()
    StoragePath = "\\Auswsr0014\shared\Aus_NI\NI Business\Tom White\Reliability Metrics Tool\Import Files Cleaned"
End Sub

' -------- Callable Methods ----------

' Public Function cleanTDBFile(strFilePath As String, strFileName As String) As String
'     cleanTDBFile = cleanMain(strFilePath, strFileName, "removeDots")
' End Function

' Public Function cleanCallDumpFile(strFilePath As String, strFileName As String) As String
'     cleanCallDumpFile = cleanMain(strFilePath, strFileName, "callDumpFilterColourRemove")
' End Function

' Public Function cleanEIDataFile(strFilePath As String, strFileName As String) As String
'     cleanEIDataFile = cleanMain(strFilePath, strFileName, "eiDataSortAndRemove")
' End Function

' Public Function cleanMaintFile(strFilePath As String, strFileName As String) As String
'     cleanEIDataFile = cleanMain(strFilePath, strFileName, "removeDots")
' End Function


'***************************************************************************
' Purpose: Used as the main skeleton of steps of cleaning and importing.
' Arguments:
'       - strFilePath As String : the path of where the file to be cleaned is
'       - strFileName As String : the type of file {"EIData", "CallDump", "TDB"}
'               It is used for the storagePath of the cleaned file.
'               So nothing super important.
'       - cleanMethod As String : the name of a specific sub that does the
'               cleaning with a excel.Worksheet as a singular argument.
' Kinzey
'***************************************************************************
Public Function cleanMain(strFilePath As String, strFileName As String, cleanMethod As String) As String   
    Dim appExcel As Excel.Application
    Dim myWorkbook As Excel.Workbook
    Dim ws As Excel.Worksheet
    
    Dim SavePath As String
    
    Call globalInit
    
    ' Setting up
    Set appExcel = New Excel.Application
    Set myWorkbook = appExcel.Workbooks.Open(strFilePath)
    appExcel.Visible = True
    Set ws = myWorkbook.Worksheets(1)

    ' --------------------------------------

    If checkColumnCountMatching(appExcel, ws, strFileName) = False Then
        Call myWorkbook.Close()

        ' Closing the excel app
        If Not (appExcel Is Nothing) Then
            appExcel.Quit
            Set appExcel = Nothing
        End If

        Call Err.Raise(vbObjectError + 513, "Different Size Import to Tbl", "When trying to import " & strFileName & " the number of columns " & _
                "in the excel file is different from the access table. Please check for missing/extra columns in the excel import.")
    End If


    ' Run the specific requirement for each file
    Call Application.Run(cleanMethod, appExcel, ws)

    ' ----------- Save the File ------------
    appExcel.DisplayAlerts = False
    
    ' Saving the file
    SavePath = StoragePath + "\" + strFileName + "Cleaned_" + Format(Date, "dd_mm_yyyy") + ".xlsx"
    Call myWorkbook.Close(True, SavePath)
    Set myWorkbook = Nothing
   
    appExcel.DisplayAlerts = True
    
    ' --------------------------------------

    ' Closing the excel app
    If Not (appExcel Is Nothing) Then
        appExcel.Quit
        Set appExcel = Nothing
    End If
    
    ' Return the location of the cleaned TDBFile
    cleanMain = SavePath

End Function

'***************************************************************************
' Purpose: Used to remove the dots from a worksheet.
' Kinzey
'***************************************************************************
Public Sub removeDots(appExcel As Excel.Application, ws As Excel.Worksheet)
    ' Remove all the dots
    Call ws.Range("A1").EntireRow.Replace(".", "")
End Sub


'***************************************************************************
' Purpose: Used because sometimes the user imports data that has columns missing.
'           E.g. sometimes people import the maintenance data without including
'           the visit type at the end
' Kinzey
'***************************************************************************
Public Function checkColumnCountMatching(appExcel As Excel.Application, ws As Excel.Worksheet, tableName As String) As Boolean
    Dim excelNumRows As Long: excelNumRows = ws.Cells(2, ws.Columns.Count).End(xlToLeft).Column + 1
    Dim accessTblNumRows As Long: accessTblNumRows = CurrentDb.TableDefs(tableName).Fields.Count

    Debug.Print "excelNumRows: " & CStr(excelNumRows)
    Debug.Print "accessTblNumRows: " & CStr(accessTblNumRows)

    If excelNumRows <> accessTblNumRows Then
        checkColumnCountMatching = False
    Else
        checkColumnCountMatching = True
    End If
End Function


'***************************************************************************
' Purpose: Used to remove the dots from a worksheet. And also format the dates
' Kinzey
'***************************************************************************
Public Sub removeDotsAndFormatDate(appExcel As Excel.Application, ws As Excel.Worksheet)

    Dim columnsToFormat As String: columnsToFormat = "N:O"
    ' Remove all the dots
    Call removeDots(appExcel, ws)
    ws.Columns(columnsToFormat).NumberFormat = "m/dd/yyyy"
End Sub


'***************************************************************************
' Purpose: Contains the specific steps for call dump
' Kinzey
'***************************************************************************
Public Sub callDumpFilterColourRemove(appExcel As Excel.Application, ws As Excel.Worksheet)
    
    ' Turning on filter
    If ws.AutoFilterMode = False Then
        ws.Range("A1").AutoFilter
    End If
    
    Call removeDots(appExcel, ws)
    
    ' filter by light yellow color
    ws.AutoFilter.Sort.SortFields.Clear
    ws.Range("A1").AutoFilter Field:=1, Criteria1:=RGB(255, 255, 153), Operator:=xlFilterCellColor

    ' Selecting all visible cells not including the header and deleting their rows
    ws.Select
    ws.Range("A1").Offset(1, 0).Select
    ws.Range(appExcel.Selection, appExcel.Selection.End(xlDown)).SpecialCells(xlCellTypeVisible).EntireRow.Delete
   
    ' filter by dark yellow color
    ws.AutoFilter.Sort.SortFields.Clear
    ws.Range("A1").AutoFilter Field:=1, Criteria1:=RGB(255, 255, 0), Operator:=xlFilterCellColor
    
    ' Selecting all visible cells not including the header and deleting their rows
    ws.Select
    ws.Range("A1").Offset(1, 0).Select
    ws.Range(appExcel.Selection, appExcel.Selection.End(xlDown)).SpecialCells(xlCellTypeVisible).EntireRow.Delete
    
    ' show the full data
    ws.Outline.ShowLevels RowLevels:=5
End Sub


'***************************************************************************
' Purpose: Contains the specific steps for Repairs. Has same steps as call dump
'       put extra step for removing leading zeros for equipment number.
' Kinzey
'***************************************************************************
Public Sub filterColourRemoveAndEquipmentNumLeadingZero(appExcel As Excel.Application, ws As Excel.Worksheet)
    ' do all the same steps as call dump, but add a additional step after
    Call callDumpFilterColourRemove(appExcel, ws)
    
    ' the leading Zeros are in column D
    Dim cell As Range
    
    For Each cell In ws.UsedRange.Columns("D").Cells
        cell.Value = Replace(LTrim(Replace(cell.Value, "0", " ")), " ", "0")
    Next cell
    
End Sub




'***************************************************************************
' Purpose: Contains the specific steps for eiData
' Kinzey
'***************************************************************************
Public Sub eiDataSortAndRemove(appExcel As Excel.Application, ws As Excel.Worksheet)
   
    ' Turning on filter
    If ws.AutoFilterMode = False Then
        ws.Range("A1").AutoFilter
    End If
    
    ws.Range("A1:Y1").Value = Array( _
                "Equipment", _
                "Technical obj type", _
                "Work Center", _
                "Service Leader WC", _
                "Last Name SL", "Name", _
                "Street", "House Number", _
                "Postal Code", _
                "City", _
                "District", _
                "Name of technician", _
                "Equipment Manufacturer", _
                "Product Lines NI/MOD/ESC", _
                "Rated Load", _
                "Rated Speed", _
                "Num of Stops", _
                "NoLanding Doors & Operat", _
                "NoCar Door and Type", _
                "Contract start date", _
                "Contract end date", _
                "Contract Type (serv prod)", _
                "Number of visits sold", _
                "Strategy / Servlevel applied", _
                "Document Currency" _
    )

    Call removeDots(appExcel, ws)

    ' Sorting newest to oldest on contract start date
    ws.AutoFilter.Sort.SortFields.Clear
    ws.AutoFilter.Sort.SortFields.Add2 Key:= _
        ws.Range("T1").EntireColumn, SortOn:=xlSortOnValues, order:=xlDescending, _
        DataOption:=xlSortNormal
    With ws.AutoFilter.Sort
        .Header = xlYes
        .MatchCase = False
        .Orientation = xlTopToBottom
        .SortMethod = xlPinYin
        .Apply
    End With
   
    ' Sorting the equipment number
    ws.AutoFilter.Sort.SortFields.Clear
    ws.AutoFilter.Sort.SortFields.Add2 Key:= _
        ws.Range("A1").EntireColumn, SortOn:=xlSortOnValues, order:=xlAscending, DataOption _
        :=xlSortNormal
    With ws.AutoFilter.Sort
        .Header = xlYes
        .MatchCase = False
        .Orientation = xlTopToBottom
        .SortMethod = xlPinYin
        .Apply
    End With
   
    ' Remove all the duplicates based on equipment number
    ws.Range("A1").CurrentRegion.RemoveDuplicates Columns:=1, Header:= _
        xlYes
   
    ' ws.AutoFilter
End Sub



