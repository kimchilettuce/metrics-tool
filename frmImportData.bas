Option Explicit
Option Compare Database

Dim defaultString As String

Dim cleanedTDBPath As String
Dim cleanedCallDumpPath As String
Dim cleanedEIPath As String


Private Sub Form_Load()
    defaultString = "< Select a File >"

    lblFirstFile.Caption = defaultString
    lblSecondFile.Caption = defaultString
    lblThirdFile.Caption = defaultString
    lblFourthFile.Caption = defaultString
    lblFifthFile.Caption = defaultString
    lblSixthFile.Caption = defaultString
    
    Call dispTimeStamp
End Sub


' ~~~~~~~~~~~ Opening FileDialogues ~~~~~~~~~~~~~~

Private Sub btnOpenFile1_Click()
    Dim ans As String: ans = openFile()
    
    ' Check that a file was selected
    If ans <> "" Then
        lblFirstFile.Caption = ans
    Else
        lblFirstFile.Caption = defaultString
    End If
    
End Sub

Private Sub btnOpenFile2_Click()
    Dim ans As String: ans = openFile()
    
    If ans <> "" Then
        lblSecondFile.Caption = ans
    Else
        lblSecondFile.Caption = defaultString
    End If
    
End Sub

Private Sub btnOpenFile3_Click()
    Dim ans As String: ans = openFile()
    
    If ans <> "" Then
        lblThirdFile.Caption = ans
    Else
        lblThirdFile.Caption = defaultString
    End If
End Sub


Private Sub btnOpenFile4_Click()
    Dim ans As String: ans = openFile()
    
    If ans <> "" Then
        lblFourthFile.Caption = ans
    Else
        lblFourthFile.Caption = defaultString
    End If
End Sub


Private Sub btnOpenFile5_Click()
    Dim ans As String: ans = openFile()
    
    If ans <> "" Then
        lblFifthFile.Caption = ans
    Else
        lblFifthFile.Caption = defaultString
    End If
End Sub


Private Sub btnOpenFile6_Click()
    Dim ans As String: ans = openFile()
    
    If ans <> "" Then
        lblSixthFile.Caption = ans
    Else
        lblSixthFile.Caption = defaultString
    End If
End Sub

' ~~~~~~~~~~~~~~~~~~ Buttons Clicked ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'***************************************************************************
' Purpose: The loop and checks and cleaning that is used for each file
' that will get imported
' Kinzey
'***************************************************************************
Private Sub cleanAndImportFile(lbl As label, fileName As String, cleanMethod As String, ByRef logMsg As String)
    
    On Error GoTo ERR_HANDLE
    
    Dim errorMsg As String: errorMsg = ""
    Dim cleanedPath As String

    If lbl.Caption <> defaultString Then

        ' Incorrect file name
        If lbl.Caption Like "*" + fileName + "*" = False Then
            errorMsg = "File does not contain [" + fileName + "] in filepath"
            Call Err.Raise(vbObjectError + 1, "Loading File", errorMsg)
           
        ' Begin cleaning the file
        Else
            ' MsgBox "TDB file detected", vbInformation
            logMsg = logMsg + fileName + " file detected" + vbNewLine
            
            ' Changing this to take it a function pointer instead
            'cleanedTDBPath = cleanTDBFile(lbl.Caption)
            ' cleanedPath = Application.Run(cleanMethod, lbl.Caption)

            ' cleanedPath = Application.Run("cleanMain", lbl.Caption, fileName, cleanMethod)
            
            cleanedPath = cleanMain(lbl.Caption, fileName, cleanMethod)


            
            logMsg = logMsg + fileName + " file cleaned" + vbNewLine
            
            ' --------- Now do the importing -------------
            
            DoCmd.SetWarnings False

            ' clearing the table first of existing data
            If fileName = "Call Dump" Then
                DoCmd.RunSQL "DELETE * FROM [Call Dump]"
            Else
                DoCmd.RunSQL "DELETE * FROM " + fileName
            End If

            Call importDataFromExcelDoCmd(fileName, cleanedPath)
            
            DoCmd.SetWarnings True

            ' MsgBox "TDB file imported"
            logMsg = logMsg + fileName + " file imported" + vbNewLine + "~~~" + vbNewLine

            ' save the timestamp after everything is completed
            Call SaveSetting("MetricsTool", "ImportData", fileName, generateTimeStamp())
        End If
    End If
    
    Exit Sub
    
ERR_HANDLE:
    MsgBox Err.Source & " - with error number: " & Err.Number _
            & " " & vbNewLine & Err.Description, vbExclamation
End Sub

'***************************************************************************
' Purpose: Used on load click, the already chosen files are first cleaned,
'   then imported into the database.
' Kinzey
'***************************************************************************
Private Sub btnLoad_Click()
    On Error GoTo ERR_HANDLE
    
    Dim logMsg As String: logMsg = ""
    
    ' gives us that loading sign
    DoCmd.Hourglass True

    ' ------------ Separate checks for correct file names ----------
    
    Call cleanAndImportFile(lblFirstFile, "TDB", "removeDots", logMsg)
    Call cleanAndImportFile(lblSecondFile, "Call Dump", "callDumpFilterColourRemove", logMsg)
    Call cleanAndImportFile(lblThirdFile, "EIData", "eiDataSortAndRemove", logMsg)
    Call cleanAndImportFile(lblFourthFile, "Maintenance", "removeDots", logMsg)
    Call cleanAndImportFile(lblFifthFile, "Repairs", "filterColourRemoveAndEquipmentNumLeadingZero", logMsg)
    Call cleanAndImportFile(lblSixthFile, "Requests", "removeDotsAndFormatDate", logMsg)
        
    DoCmd.Hourglass False
    
    ' Print out the log msg of actions completed, if not empty
    If logMsg <> "" Then
        MsgBox logMsg, vbInformation
    End If
    
    ' calls own self form load
    Call Form_Load
    
    Exit Sub

ERR_HANDLE:
   
    MsgBox Err.Number & ": " & Err.Description, vbCritical
    DoCmd.Hourglass False

    ' to reset all the default strings
    Call Form_Load

    ' end the sub here on error
    Exit Sub
End Sub


' Used for when back button is clicked
Private Sub btnBack_Click()
    Call DoCmd.Hourglass(False)
    DoCmd.Close acForm, "frmImportData", acSaveYes
    Forms!frmMTBCMainMenu.Visible = True
End Sub

' ~~~~~~~~~~~~~~~~~~ Helper Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'***************************************************************************
' Purpose: Updates the value of the file timestamps.
' Kinzey
'***************************************************************************
Sub dispTimeStamp()
    lblFile1TimeStamp.Caption = GetSetting("MetricsTool", "ImportData", "TDB", Default:="")
    lblFile2TimeStamp.Caption = GetSetting("MetricsTool", "ImportData", "Call Dump", Default:="")
    lblFile3TimeStamp.Caption = GetSetting("MetricsTool", "ImportData", "EIData", Default:="")
    lblFile4TimeStamp.Caption = GetSetting("MetricsTool", "ImportData", "Maintenance", Default:="")
    lblFile5TimeStamp.Caption = GetSetting("MetricsTool", "ImportData", "Repairs", Default:="")
    lblFile6TimeStamp.Caption = GetSetting("MetricsTool", "ImportData", "Requests", Default:="")
End Sub

'***************************************************************************
' Purpose: Each file has a timestamp of last import. This function determines
'           what you want the stamp to display.
' Kinzey
'***************************************************************************
Function generateTimeStamp() As String
    generateTimeStamp = "Last update time: " + Format(Now, "hh:mm:ss AM/PM | dd mmmm | yyyy")
End Function

'==========================================
' Import Data From Excel With DoCmd
'==========================================
Sub importDataFromExcelDoCmd(strTableName As String, strFileName As String)

    ' Set variables
    Dim blnHasHeadings As Boolean: blnHasHeadings = True
    
    ' Import data
    DoCmd.TransferSpreadsheet acImport, acSpreadsheetTypeExcel12, strTableName, strFileName, blnHasHeadings
End Sub


