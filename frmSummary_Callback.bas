Option Compare Database


'***************************************************************************
' Purpose: Used to hide the subform for requests if there are no repair
' requests associated with the callback
' Kinzey
'***************************************************************************
Private Sub Form_Current()

    Me.InsideWidth = 16000
    lblNoRequests.Visible = False   ' by default want it to be hidden

    With Me![Requests].Form
     
        ' Check the RecordCount of the Subform.
        If .RecordsetClone.RecordCount = 0 Then
         
            ' Hide the subform.
            .Visible = False

            ' Want to display a label that lets the user know there are no requests
            lblNoRequests.Visible = True
        End If
    End With
    
End Sub

Private Sub setDefaultFormSize()
    With Me
        ' .InsideHeight = 2200
        .InsideWidth = 10000
    End With
End Sub
