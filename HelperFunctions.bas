Option Compare Database
Option Explicit

' Used to open the fileDialogue and retrieve the path string for the file.
Function openFile() As String
    On Error GoTo OPENFILE_ERR

    Dim fd As Office.FileDialog
    Dim strFile As String: strFile = ""
     
    Set fd = Application.FileDialog(msoFileDialogFilePicker)
     
    With fd
     
        .Filters.Clear
        .Filters.Add "Excel Files", "*.xls*", 1             ' allows normal, macro-enabled, binary excel files
        .title = "Choose the Weekly Report"                 ' todo: add msg
        .AllowMultiSelect = False
     
        .InitialFileName = "\\Auswsr0014\shared\Aus_NI\NI Business\Tom White\Reliability Metrics Tool"
     
        If .Show = True Then
     
            strFile = .SelectedItems(1)
     
        End If
     
    End With

    openFile = strFile


OPENFILE_EXIT:
    Exit Function

OPENFILE_ERR:
    MsgBox Error$
    Resume OPENFILE_EXIT

End Function


Public Function convert01ToBoolean(num As Integer) As Boolean
    If num <> 0 Then
        convert01ToBoolean = True
    Else
        convert01ToBoolean = False
    End If
End Function


'***************************************************************************
' Purpose: Used to compare two version control strings and returns a negative
'           integer if semVer1 < semVer2, and returns 0 if semVer1 = semVer2,
'           and returns a positive integer if semVer1 > semVer2.
' Kinzey
'***************************************************************************
Public Function compareSemVer(ByVal semVer1 As String, ByVal semVer2 As String) As Integer
    Dim splitVer1() As String
    Dim splitVer2() As String

    If semVer1 = semVer2 Then
        compareSemVer = 0
        Exit Function
    End If

    ' Remove the starting v
    semVer1 = Right(semVer1, 5)
    semVer2 = Right(semVer2, 5)

    semVer1 = Replace(semVer1, ".", "")
    semVer2 = Replace(semVer2, ".", "")
    
    compareSemVer = CInt(semVer1) - CInt(semVer2)
End Function


' -------------------------------------------------- Settings Subs --------------------------------------------------------------

'***************************************************************************
' Purpose: Used to replace the entries of tblSettings with tblSettings_DEFAULT_VALUES
' Kinzey
'***************************************************************************
Public Sub resetSettingDefaultsRunSQL()
    ' Needed because deleting entries from table raises a warning
    DoCmd.SetWarnings False

    ' Clear the old chosen settings by a user, and set them back to their defaults
    DoCmd.RunSQL ("DELETE * FROM tblSettings")
    DoCmd.RunSQL ("SELECT * INTO tblSettings FROM tblSettings_DEFAULT_VALUES")

    DoCmd.SetWarnings True
End Sub


'***************************************************************************
' Purpose: Helper sub to run a SQL command to update the user selections in
'           this form to go into tblSettings
' Kinzey
'***************************************************************************
Public Sub updateSettings(settingName As String, settingValue As String)
    Dim strTable As String: strTable = "tblSettings"

    DoCmd.SetWarnings False
    DoCmd.RunSQL ("UPDATE " & strTable & " SET [User Selection] = '" & settingValue & "' WHERE [Setting Type] = '" & settingName & "'")
    DoCmd.SetWarnings True

End Sub


'***************************************************************************
' Purpose: Helper sub to retrieve a setting from tblSettings
' Kinzey
'***************************************************************************
Public Function getSettings(settingName As String) As String
    Dim strTable As String: strTable = "tblSettings"

    getSettings = DLookup("[User Selection]", strTable, "[Setting Type] = '" & settingName & "'")
End Function
