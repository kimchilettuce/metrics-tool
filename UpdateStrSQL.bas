Dim strSQL As String

'***************************************************************************
' Purpose: Used to dynmically change the SQL string to have hte crtieria and
' based on the ordertype, order in according to orderField
' Kinzey
'***************************************************************************
Private Sub applyCrtieriaSortOnSQL(ByRef strSQL As String, qryName As String, Criteria As String, orderType As String, orderField As String)

    If LCase(orderType) = "alphabetical" Then
        strSQL = strSQL & " ORDER BY "

    ElseIf LCase(orderType) = "highlow" Then
        strSQL = strSQL & " ORDER BY " & orderField & " DESC, "

    ElseIf LCase(orderType) = "lowhigh" Then
        strSQL = strSQL & " ORDER BY " & orderField & " ASC, "
    
    Else
        ' not a valid orderType
        Err.Raise vbObjectError + 513

    End If

    ' this follows the "ORDER BY " from above, hence adds another criteria to order by.
    '       whether orderType be alphabetical, highlow or lowhigh,
    '       at the end still want to sort by criteria.
    strSQL = strSQL & qryName & "." & Criteria & ";"

End Sub


'***************************************************************************
' Purpose: Deletes existing strSQl, and replaces it with the new strSQL
' Kinzey
'***************************************************************************
Public Sub replaceStrSQL(qryName As String, strSQL As String)
    ' the set qdef may be unnecessary
    CurrentDb().QueryDefs.Delete qryName
    Set qdef = CurrentDb().CreateQueryDef(qryName, strSQL)
End Sub


'***************************************************************************
' Purpose: This is a helper function for when we want to order a SQL by its
'           2nd and 3rd column. However the "order" plays a part in the middle
'           word being either ASC or DESC.
'
'           Don't worry too much about this function. I just had a slight problem
'           and I needed this function to fix it.
' Kinzey
'***************************************************************************
Private Function generateOrder2and3(order As String) As String

    ' The orderfield here is "2", meaning to order it based on the second field
    '           https://stackoverflow.com/questions/89820/how-to-reference-a-custom-field-in-sql
    If LCase(order) = "lowhigh" Then
        generateOrder2and3 = "2 ASC, 3"
    ElseIf LCase(order) = "highlow" Then
        generateOrder2and3 = "2 DESC, 3"
    ElseIf LCase(order) = "alphabetical" Then
        generateOrder2and3 = ""
    Else
        ' not a valid orderType
        Err.Raise vbObjectError + 513
    End If
End Function


'***************************************************************************
' Purpose:
'           Note that if we using the criteria of "Technician number" then the
'           first column will be grabbed from tblPersonnelNumToName, rather than
'           from [baseQry].[Criteria]. Similarly the FROM statement would include
'           an inner join with tblPersonnelNumToName if the criteria is "Technician number".
'           Hence, this sub is to reduce the amount of repeated code and helps
'           generate the dynamic strSQL.
'
' Parameters:
'           Criteria    (String)    :   Used to determine if the case if "[Technician number]" or not.
'           baseQry     (String)    :   The name of the qry that the _graph qry is
'                                       running off.
'           firstColumn (String)    :   This is a variable by reference that
'                                       can be passed as an empty string,
'                                       and at the end will contain the correctly
'                                       generated strSQL
'           fromTable   (String)    :   Similar to above. but this is the
'                                       strSQL for the FROM statement
' Kinzey
'***************************************************************************
Sub getSQLDependingOnIfTechnicianNumber(ByRef Criteria As String, ByRef baseQry As String, ByRef firstColumn As String, fromTable As String)
    If Criteria = "[Technician number]" Then
        firstColumn = "tblPersonnelNumToName.[Attending Tech Name]"
        fromTable = "FROM " & baseQry & " INNER JOIN tblPersonnelNumToName ON " & baseQry & ".[Technician number] = tblPersonnelNumToName.[Attending Tech Personnel Number] "

        ' Update these values since they are ByRef
        Criteria = "[Attending Tech Name]"
        baseQry = "tblPersonnelNumToName"
    Else
        firstColumn = baseQry & "." & Criteria
        fromTable = "FROM " & baseQry & " "
    End If
End Sub

' ---------------------------------------------------------------------------------------------------------------------------------------

Sub QryTDB(strFilter As String)
    Dim normalArrivalTime As String: normalArrivalTime = getSettings("Call Back: Normal Arrival Time")
    Dim normalLeaveTime As String: normalLeaveTime = getSettings("Call Back: Normal Leave Time")

    strSQL = "SELECT UNITS.ID, QryCallDumpApplyDateFilter.CallID, UNITS.Equipment, UNITS.[Equipment Manufacturer], UNITS.[Contract Type], UNITS.[Service Leader WC], UNITS.[Last Name SL], " & _
            "UNITS.[Service Run], UNITS.[Product Lines NI/MOD/ESC], QryCallDumpApplyDateFilter.[Contract status (active/passive)], QryCallDumpApplyDateFilter.Notification, QryCallDumpApplyDateFilter.[Notification date], " & _
            "QryCallDumpApplyDateFilter.[Notification Time], QryCallDumpApplyDateFilter.[Arrival Date], QryCallDumpApplyDateFilter.[Arrival Time], QryCallDumpApplyDateFilter.[Departure Date], QryCallDumpApplyDateFilter.[Departure Time], " & _
            "QryCallDumpApplyDateFilter.[Caller Description], DateDiff(""n"",[Arrival Time],[Departure Time])+DateDiff(""n"",[Arrival Date],[Departure Date]) AS Minutes, " & _
            "QryCallDumpApplyDateFilter.Description, QryCallDumpApplyDateFilter.[Long Text Description], QryCallDumpApplyDateFilter.[Tech Last Name], QryCallDumpApplyDateFilter.[Technician number], UNITS.[Rated load], " & _
            "UNITS.[Rated Speed], UNITS.[Num of Stops], QryCallDumpApplyDateFilter.[SCS A group], QryCallDumpApplyDateFilter.[Car shut down], Format([arrival time],""Short Time"") AS [Arr Time], " & _
            "IIF([notification date] is null,"""",IIf((Weekday([arrival date],1) between 2 and 6) And ([arr time] Between " & Chr(34) & normalArrivalTime & Chr(34) & " And " & Chr(34) & normalLeaveTime & Chr(34) & "),""Normal"",""OT"")) AS Overtime, " & _
            "IIF([notification date] is null,"""",IIf((Weekday([arrival date],1) between 2 and 6),""Weekday"",""Weekend"")) AS Weekday, " & _
            "IIF([notification date] is null,"""",IIf([arr time] Between " & Chr(34) & normalArrivalTime & Chr(34) & " And " & Chr(34) & normalLeaveTime & Chr(34) & ",""Normal"",""After hours"")) AS [After Hours] " & _
            "FROM UNITS LEFT JOIN QryCallDumpApplyDateFilter ON UNITS.Equipment = QryCallDumpApplyDateFilter.Equipment " & _
            "GROUP BY UNITS.ID, QryCallDumpApplyDateFilter.CallID, UNITS.Equipment, UNITS.[Equipment Manufacturer], UNITS.[Contract Type], UNITS.[Service Leader WC], " & _
            "UNITS.[Last Name SL], UNITS.[Service Run], UNITS.[Product Lines NI/MOD/ESC], QryCallDumpApplyDateFilter.[Contract status (active/passive)], " & _
            "QryCallDumpApplyDateFilter.Notification, QryCallDumpApplyDateFilter.[Notification date], QryCallDumpApplyDateFilter.[Notification Time], QryCallDumpApplyDateFilter.[Arrival Date], QryCallDumpApplyDateFilter.[Arrival Time], " & _
            "QryCallDumpApplyDateFilter.[Departure Date], QryCallDumpApplyDateFilter.[Departure Time], QryCallDumpApplyDateFilter.[Caller Description], QryCallDumpApplyDateFilter.Description, QryCallDumpApplyDateFilter.[Long Text Description], " & _
            "QryCallDumpApplyDateFilter.[Tech Last Name], QryCallDumpApplyDateFilter.[Technician number], UNITS.[Rated load], UNITS.[Rated Speed], UNITS.[Num of Stops], QryCallDumpApplyDateFilter.[SCS A group], " & _
            "QryCallDumpApplyDateFilter.[Car shut down], IIf((Weekday([arrival date],1) between 2 and 6),""Weekday"",""Weekend"") " & _
            "HAVING ( ([UNITS].[Equipment]is not null)"

    ' Add the filtering SQL at the end of the above statement
    strSQL = strSQL & strFilter

    Call replaceStrSQL("QryTDB", strSQL)
End Sub


' ---------------------------------------------------------------------------------------------------------------------------------------

Sub QryCountNotifications(Criteria As String)
    strSQL = "SELECT QryTDB." & Criteria & ", Sum(IIf([SCS A group]='T',1,0)) AS T, Sum(IIf([SCS A group]='E',1,0)) AS E" & _
                " FROM QryTDB" & _
                " WHERE (((QryTDB.[Notification date])>=[Forms]!frmMTBCMainMenu![txtBeginDate] And (QryTDB.[Notification date])<=[Forms]!frmMTBCMainMenu![txtEndDate]))" & _
                " GROUP BY QryTDB." & Criteria & _
                " ORDER BY Sum(IIf([SCS A group]='T',1,0)) DESC;"

    Call replaceStrSQL("QryCountNotifications", strSQL)
End Sub


Sub QryCountNotifications_Graph(Criteria As String, order As String)

    Dim firstColumn As String
    Dim fromTable As String
    Dim baseQry As String: baseQry = "QryCountNotifications"

    ' we want a copy of the value of Criteria. We don't want to edit it within the sub
    Dim localCriteria As String: localCriteria = Criteria

    Call getSQLDependingOnIfTechnicianNumber(localCriteria, baseQry, firstColumn, fromTable)


    strSQL = "SELECT TOP 10 " & firstColumn & ", QryCountNotifications.T, QryCountNotifications.E " & _
                fromTable & _
                "GROUP BY " & firstColumn & ", QryCountNotifications.T, QryCountNotifications.E"

    Call applyCrtieriaSortOnSQL(strSQL, baseQry, localCriteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountNotifications_graph", strSQL)
End Sub


Sub QryCountShutdowns(Criteria As String)

    ' TW 150322: modified below SQL so that;
    '   1) Minutes <= 240 is a condition for all fields (previosuly was only for the count of calls, not the shutdowns)
    '   2) Shutdown (%) forumlas reference previous fields, instead of calculating all over again.
    strSQL = "SELECT QryTDB." & Criteria & ",  Sum(IIf([Car shut down]='Y' And [overtime]=""normal"" And [Minutes]<=240,1,0)) AS [Shutdowns (norm)]," & _
                " Sum(IIf([Car shut down]='N' And [overtime]=""normal"" And [Minutes]<=240,1,0)) AS [Back in Serv (norm)]," & _
                " IIf([Back in Serv (norm)]+[Shutdowns (norm)]=0,0,Round(100*[Shutdowns (norm)]/([Shutdowns (norm)]+[Back in Serv (norm)]),1)) AS [Normal Time (%)]," & _
                " Sum(IIf([Car shut down]='Y' And [overtime]=""OT"" And [Minutes]<=240,1,0)) AS [Shutdowns (Aft Hrs)]," & _
                " Sum(IIf([Car shut down]='N' And [overtime]=""OT"" And [Minutes]<=240,1,0)) AS [Back in Serv (Aft Hrs)]," & _
                " IIf([Back in Serv (Aft Hrs)]+[Shutdowns (Aft Hrs)]=0,0,Round(100*[Shutdowns (Aft Hrs)]/([Shutdowns (Aft Hrs)]+[Back in Serv (Aft Hrs)]),1)) AS [After Hours (%)]" & _
                " FROM QryTDB" & _
                " WHERE (((QryTDB.[Notification date])>=[Forms]!frmMTBCMainMenu![txtBeginDate] And (QryTDB.[Notification date])<=[Forms]!frmMTBCMainMenu![txtEndDate]) AND ((QryTDB.[SCS A group])='T'))" & _
                " GROUP BY QryTDB." & Criteria & "" & _
                " ORDER BY Sum(IIf([Car shut down]='Y' And [overtime]=""normal"",1,0)) DESC;"

    Call replaceStrSQL("QryCountShutdowns", strSQL)
End Sub


Sub QryCountShutdowns_Graph(Criteria As String, order As String)

    Dim firstColumn As String
    Dim fromTable As String
    Dim baseQry As String: baseQry = "QryCountShutdowns"

    ' we want a copy of the value of Criteria. We don't want to edit it within the sub
    Dim localCriteria As String: localCriteria = Criteria

    Call getSQLDependingOnIfTechnicianNumber(localCriteria, baseQry, firstColumn, fromTable)


    strSQL = "SELECT TOP 10 " & firstColumn & ", QryCountShutdowns.[Normal Time (%)], QryCountShutdowns.[After Hours (%)] " & _
                fromTable & _
                "GROUP BY " & firstColumn & ", QryCountShutdowns.[Normal Time (%)], QryCountShutdowns.[After Hours (%)]"

    Call applyCrtieriaSortOnSQL(strSQL, baseQry, localCriteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountShutdowns_graph", strSQL)
End Sub


Sub QryCountCallDuration(Criteria As String)

    Dim onSiteOvertime As String: onSiteOvertime = getSettings("Call Back: On-site Outlier (mins)")

    strSQL = "SELECT QryTDB." & Criteria & ", Sum(IIf([overtime]=""normal"" AND [Minutes] <= " & onSiteOvertime & ",1,0)) AS [Calls (norm)], " & _
                "Sum(IIf([overtime]=""normal"" And [Minutes]<= " & onSiteOvertime & ",[Minutes],0)) AS [Sum of Minutes (norm)], " & _
                "IIf([Calls (norm)]=0,0,Round([Sum of Minutes (norm)]/[Calls (norm)],1)) AS [Average Minutes (norm)], " & _
                "Sum(IIf([overtime]=""OT"" AND [Minutes] <= " & onSiteOvertime & ",1,0)) AS [Calls (OT)], " & _
                "Sum(IIf([overtime]=""OT"" And [Minutes]<= " & onSiteOvertime & ",[Minutes],0)) AS [Sum of Minutes (OT)], " & _
                "IIf([Calls (OT)]=0,0,Round([Sum of Minutes (OT)]/[Calls (OT)],1)) AS [Average Minutes (OT)] " & _
                "FROM QryTDB " & _
                "WHERE (((QryTDB.[Notification date])>=[Forms]!frmMTBCMainMenu![txtBeginDate] And (QryTDB.[Notification date])<=[Forms]!frmMTBCMainMenu![txtEndDate]) AND " & _
                "((QryTDB.[SCS A group])='T')) " & _
                "GROUP BY QryTDB." & Criteria & ";"
    
    Call replaceStrSQL("QryCountCallDuration", strSQL)
End Sub


Sub QryCountCallDuration_graph(Criteria As String, order As String)

    Dim firstColumn As String
    Dim fromTable As String
    Dim baseQry As String: baseQry = "QryCountCallDuration"

    ' we want a copy of the value of Criteria. We don't want to edit it within the sub
    Dim localCriteria As String: localCriteria = Criteria

    Call getSQLDependingOnIfTechnicianNumber(localCriteria, baseQry, firstColumn, fromTable)


    strSQL = "SELECT TOP 10 " & firstColumn & ", QryCountCallDuration.[Average Minutes (norm)], QryCountCallDuration.[Average Minutes (OT)] " & _
                fromTable & _
                "GROUP BY " & firstColumn & ", QryCountCallDuration.[Average Minutes (norm)], QryCountCallDuration.[Average Minutes (OT)]"

    Call applyCrtieriaSortOnSQL(strSQL, baseQry, localCriteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountCallDuration_graph", strSQL)
End Sub







'***************************************************************************
' Purpose: This is a special case for the metric FirstTimeFix. This is because
'           there is a requirement to check if a particular entry is within dayDiff days
'           from the previous entry in the table. This can't be done using SQL
'           but must be done using VBA.
'
'           And so we read the results from QryCountFirstTimeFix that sets up
'           a result that we loop through, and then we place the result into
'           tblFirstTimeFix.
'
'           Lastly QryCountFirstTimeFix_graph will then read from tblFirstTimeFix
'           and format it correctly and make it ready to be graphed.
'
'           Note: This sub only needs to be called when the value of the parameters changes
'                   there is no need to compute the same thing again and again.
'
' Parameters:
'           dayDiff         (Integer)       : The number of days between two calls that triggers
'                                                a "N" first time fix.
'           includeECall    (Boolean)       : An option whether the user wants the count
'                                                to include E Calls or only T Calls.
'
' Kinzey
'***************************************************************************
Public Sub DoRecordByRecord_logic(Criteria As String, qryName As String, tableName As String)
    Dim rsRead As DAO.Recordset
    Dim rsWrite As DAO.Recordset

    Dim qdef As DAO.QueryDef
    Dim strSQL As String

    ' ----------- Step 1: Delete the previous entries to tblFirstTimeFix --------------
    DoCmd.SetWarnings False
    DoCmd.RunSQL ("DELETE * FROM " & tableName)
    DoCmd.SetWarnings True

    ' Here we want to trim the square brackets from the variable Criteria
    '       e.g. we want "[Service Leader]" to instead be "Service Leader"
    If qryName = "QryCountSick_Setup" Then
        ' try and find the name of first column so we can replace it
        Dim newColumnName As String

        ' Trim the first and last character of string
        newColumnName = Right(Criteria, Len(Criteria) - 1)
        newColumnName = Left(newColumnName, Len(newColumnName) - 1)

        CurrentDb.TableDefs("tblSickUnits").Fields(0).Name = newColumnName
    End If


    ' ---------- Step 2: Retrieve the setup from QryCountFirstTimeFix ---------------

    Set qdef = CurrentDb.QueryDefs(qryName)
    qdef.Parameters.Refresh
    qdef.Parameters("Forms!frmMTBCMainMenu!txtBeginDate").Value = Forms!frmMTBCMainMenu!txtBeginDate
    qdef.Parameters("Forms!frmMTBCMainMenu!txtEndDate").Value = Forms!frmMTBCMainMenu!txtEndDate
    Set rsRead = qdef.OpenRecordset

    ' open the recordset where we will place the results
    Set rsWrite = CurrentDb.OpenRecordset(tableName)

    ' ---------- Step 3: Setup the next phase of reading from QryCountFirstTimeFix and appending to tblFirstTimeFix ---------------
    
    Dim Count As Integer: Count = 0
    Dim prevEquipNum As String: prevEquipNum = "default"
    Dim prevDate As Date: prevDate = "1/1/1800"
    Dim prevIsSick As Boolean: prevIsSick = False

    'Check to see if the recordset actually contains rows
    If Not (rsRead.EOF And rsRead.BOF) Then

        rsRead.MoveFirst 'Unnecessary in this case, but still a good habit. Moves to first entry
        Do Until rsRead.EOF = True ' Or Count = 10

            If qryName = "QryCountFirstTimeFix" Then
                Call doLogic_FirstTimeFix(rsRead, rsWrite, prevEquipNum, prevDate)
            ElseIf qryName = "QryCountSick_Setup" Then
                Call doLogic_SickUnits(newColumnName, rsRead, rsWrite, prevIsSick)
            End If

            ' -------- Iterate to next entry ----------
            rsRead.MoveNext
            Count = Count + 1
        Loop

    Else
        MsgBox "There are no records in the recordset."
    End If

    rsRead.Close
    rsWrite.Close

    ' Cleanup
    Set rsWrite = Nothing
    Set rsRead = Nothing
End Sub


'***************************************************************************
' Purpose: This is abstracted out logic from the sub above called DoRecordByRecord_logic
'
' Parameters:
'           dayDiff         (Integer)       : The number of days between two calls that triggers
'                                                a "N" first time fix.
'           includeECall    (Boolean)       : An option whether the user wants the count
'                                                to include E Calls or only T Calls.
'
' Kinzey
'***************************************************************************
Sub doLogic_FirstTimeFix(ByRef rsRead As DAO.Recordset, ByRef rsWrite As DAO.Recordset, ByRef prevEquipNum As String, _
     ByRef prevDate As Date)

    Dim valueFirstTimeFix As String
    Dim dayDiff As Integer: dayDiff = DLookup("[User Selection]", "tblSettings", "[Setting Type] = 'FTF Day Diff'")

    ' --------- The Core of the logic -----------------------
    
    If IsNull(rsRead.Fields("Departure Date")) = True Then
        valueFirstTimeFix = ""
    Else
        ' first, only check if this row has same equip num as previous
        ' second, if the prev and current row have a day difference within the variable dayDiff, then set value to "Y"
        If rsRead.Fields("Equipment Number") = prevEquipNum And Abs(DateDiff("d", prevDate, rsRead.Fields("Departure Date"))) <= dayDiff Then
            valueFirstTimeFix = "N"
        Else
            valueFirstTimeFix = "Y"
        End If
    End If
    
    ' Generate the new entry to put into tblFirstTimeFix
    rsWrite.AddNew
    rsWrite("Technician number").Value = rsRead.Fields("Technician number")
    rsWrite("Equipment Number").Value = rsRead.Fields("Equipment Number")
    rsWrite("Notification Date").Value = rsRead.Fields("Notification Date")
    rsWrite("Departure Date").Value = rsRead.Fields("Departure Date")
    rsWrite("Call Type").Value = rsRead.Fields("Call Type")
    rsWrite("First Time Fix").Value = valueFirstTimeFix
    rsWrite("Debug: DayDiff").Value = dayDiff
    rsWrite.Update

    ' Update variables storing previous results
    prevEquipNum = rsRead.Fields("Equipment Number")
    prevDate = rsRead.Fields("Notification Date")
End Sub



Sub doLogic_SickUnits(criteriaStripped As String, ByRef rsRead As DAO.Recordset, ByRef rsWrite As DAO.Recordset, ByRef previousSick As Boolean)
    
    ' Here i set the value to 29 instead of 30, because we want to include the currentDate as part of the "day of"
    ' This can be changed to 30 if the implementation changes on whether you want to include the day itself.
    Const dayDiffSick As Integer = 29

    ' Bookmark is used to keep track of the position of an entry
    Dim bookmark As Variant
    Dim numLast30 As Integer: numLast30 = 1

    Dim currentEquipNum As String
    Dim currentDate As Date
    
    bookmark = rsRead.bookmark
    currentEquipNum = rsRead.Fields("Equipment Number")
    currentDate = rsRead.Fields("Notification date")
    
    ' Go back and count the number of entries less than 30 days ago
    rsRead.MovePrevious
    Do While True
        ' If reached begining of file, then exit while loop
        If rsRead.BOF Then
            Exit Do
        ElseIf rsRead.Fields("Equipment Number") <> currentEquipNum Then
            Exit Do
        ElseIf rsRead.Fields("Notification date") < DateAdd("d", -dayDiffSick, currentDate) Then
            Exit Do
        Else
            numLast30 = numLast30 + 1
        End If

        rsRead.MovePrevious
    Loop

    ' move the read recordset back to where it started
    Call rsRead.Move(0, bookmark)

    ' Generate the new entry to put into tblFirstTimeFix
    rsWrite.AddNew
    rsWrite(criteriaStripped).Value = rsRead.Fields(criteriaStripped)
    rsWrite("Equipment Number").Value = rsRead.Fields("Equipment Number")
    rsWrite("Notification Date").Value = rsRead.Fields("Notification Date")
    rsWrite("SCS A Group").Value = rsRead.Fields("SCS A Group")
    rsWrite("Num Of Calls Last 30 Days").Value = numLast30

    ' -------------------------- Check if Next Equipment Number Different --------------------------------------

    ' Here we call a function to apply the logic and check for whether or not an entry is the last entry for
    '       a particular equipment number.
    ' If it is the last entry, then apply the logic and exit the sub since the work is handled.
    '       Else continue with other cases and their logic.
    If doLogic_SickUnits_lastEquipmentNumEntry(rsRead, rsWrite, previousSick, currentEquipNum, currentDate, numLast30) = True Then Exit Sub
   
    ' ------------------------------------------------------------------------------

    Dim isSickValue As String: isSickValue = ""

    ' if the previous was sick, we only need need to check if there was a single previous
    '       T call in previous 30 days to continue to make it sick
    If previousSick = True Then
        If numLast30 >= 2 Then isSickValue = "SICK_CONTINUE"

    ' Separate logic if previous was just blank
    ElseIf previousSick = False And numLast30 >= 3 Then
        isSickValue = "SICK"
    End If

    rsWrite("isSick").Value = isSickValue
    ' ------------------------------------------------------------------------------

    ' Setting the value of previousSick for the next entry
    If isSickValue = "SICK" Or isSickValue = "SICK_CONTINUE" Then previousSick = True Else previousSick = False
   
    rsWrite.Update
End Sub



Function doLogic_SickUnits_lastEquipmentNumEntry(ByRef rsRead As DAO.Recordset, ByRef rsWrite As DAO.Recordset, _
    ByRef previousSick As Boolean, currentEquipNum As String, currentDate As Date, numLast30 As Integer) As Boolean

    Dim nextEquipNumber As String
    Dim isLastEquipmentEntry As Boolean: isLastEquipmentEntry = False
    Dim isSickValue As String: isSickValue = ""

    ' this variable is used to check if this last entry is 30 days less than today
    Const dayDiffSick As Integer = 29

    rsRead.MoveNext

    ' ---- Determine If This Entry is Last of Its Equipment Number ----

    ' Here we read the next equipment number and check if it is different from the current equipment number
    '       if it is different, then the entry is the last of its equipment number
    '       we then apply the specific logic for the last entry of a equipment number
    ' Note: we also check for if rsRead has reached End Of File, since the last entry has no next entry to read the
    '       Equipment Number.
    If rsRead.EOF = True Then
        isLastEquipmentEntry = True
    ElseIf rsRead.Fields("Equipment Number") <> currentEquipNum Then
        isLastEquipmentEntry = True
    End If

    ' -------------------- Core of the Logic ------------------------

    If isLastEquipmentEntry = True Then
        
        ' To be considered sick needs for the date to within 30 days from the enddate from the main menu
        If currentDate >= DateAdd("d", -dayDiffSick, Forms!frmMTBCMainMenu!txtEndDate) Then
            ' One case is, for the last to be considered Sick, it needs the previous to be Sick and 
            '       It needs to be 30 days from the previous to make it continue to be Sick (numLast30 >= 2)
            If previousSick = True And numLast30 >= 2 Then
                isSickValue = "SICK_LAST"
            ' Else the other case is where it just 3 or more T calls
            ElseIf previousSick = False And numLast30 >= 3 Then
                isSickValue = "SICK_LAST"
            End If

            rsWrite("isSick").Value = isSickValue
            rsWrite.Update

            ' Setup for the next entry which is a new equipment number
            previousSick = False

        End If

    End If

    ' ---------------------------------------------------------------

    ' We want to pass the boolean value of whether this entry was the lastEquipmentEntry
    '       So that in the main logic sub we can determine whether or not to continue or exit since the logic
    '       for this entry was already done.
    doLogic_SickUnits_lastEquipmentNumEntry = isLastEquipmentEntry
    rsRead.MovePrevious

End Function




Sub QryCountFirstTimeFix(Criteria As String)
    Dim whereFilter As String

    ' [Want to grab settings for including E Calls]: The DLookup returns either 1 or 0
    If DLookup("[User Selection]", "tblSettings", "[Setting Type] = 'FTF Include E Calls'") = 1 Then
        whereFilter = "WHERE ((QryTDB.[SCS A group])=""T"" OR (QryTDB.[SCS A group])=""E"") "
    Else
        whereFilter = "WHERE (((QryTDB.[SCS A group])=""T"")) "
    End If

    ' setup the strSQl for QryCountFirstTimeFix
    strSQL = "SELECT QryTDB." & Criteria & ", QryTDB.Equipment AS [Equipment Number], QryTDB.[Notification date], QryTDB.[Departure Date], QryTDB.[SCS A group] AS [Call Type] " & _
                "FROM QryTDB " & whereFilter & _
                "GROUP BY QryTDB." & Criteria & ", QryTDB.Equipment, QryTDB.[Notification date], QryTDB.[Departure Date], QryTDB.[SCS A group], QryTDB.Notification " & _
                "ORDER BY QryTDB.Equipment DESC , QryTDB.[Notification date] DESC;"

    Call replaceStrSQL("QryCountFirstTimeFix", strSQL)
End Sub


'***************************************************************************
' Parameters:
'           Criteria (String)   :   Currently not necessary since first time
'                                   fix needs only be calcualted for attending tech.
' Kinzey
'***************************************************************************
Sub QryCountFirstTimeFix_graph(Criteria As String, order As String)

    If Criteria <> "[Technician number]" Then
        Exit Sub
    End If

    strSQL = "SELECT TOP 10 tblPersonnelNumToName.[Attending Tech Name], Count(IIf([First Time Fix]=""Y"",1)) AS YesCount, Count(IIf([First Time Fix]=""N"",1)) AS NoCount " & _
                "FROM tblFirstTimeFix INNER JOIN tblPersonnelNumToName ON tblFirstTimeFix.[Technician number] = tblPersonnelNumToName.[Attending Tech Personnel Number] " & _
                "GROUP BY tblPersonnelNumToName.[Attending Tech Name]"


    Call applyCrtieriaSortOnSQL(strSQL, "tblPersonnelNumToName", "[Attending Tech Name]", order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountFirstTimeFix_graph", strSQL)
End Sub


Sub QryCountFirstTimeFix_graph_all(Criteria As String, order As String)

    If Criteria <> "[Technician number]" Then
        Exit Sub
    End If

    strSQL = "SELECT tblFirstTimeFix." & Criteria & ", Count(IIf([First Time Fix]=""Y"",1)) AS YesCount, Count(IIf([First Time Fix]=""N"",1)) AS NoCount " & _
                "FROM tblFirstTimeFix " & _
                "GROUP BY tblFirstTimeFix." & Criteria

    Call applyCrtieriaSortOnSQL(strSQL, "tblFirstTimeFix", Criteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountFirstTimeFix_graph_all", strSQL)
End Sub


' -----------------------------------------------------------------------------------------------------------------------------------------

Sub QryCountMaint_BaseData(Criteria As String, keepNotifFilter As Boolean)

    Dim WhereByStr As String

    ' -------------------------- Modifying Based on Criteria Chosen --------------------------------------------

    ' we don't want to pull the equipment field from QryTDB, if we already have on in the maintenance table
    If Criteria = "[Equipment]" Then
        strSQL = "SELECT DISTINCT "
    ElseIf Criteria = "[Technician number]" Then
        strSQL = "SELECT DISTINCT Maintenance.[Personnel number] AS " & Criteria & ", "
    Else
        strSQL = "SELECT DISTINCT QryTDB." & Criteria & ", "
    End If

    ' ----------------------------- Modifying based on notification date ---------------------------------------

    If keepNotifFilter = True Then
        WhereByStr = "WHERE (((Maintenance.[Planned date])>=[Forms]![frmMTBCMainMenu]![txtBeginDate] And (Maintenance.[Planned date])<=[Forms]![frmMTBCMainMenu]![txtEndDate]));"
    Else
        WhereByStr = ";"
    End If

    ' ----------------------------------------------------------------------------------------------------------

    strSQL = strSQL & "Maintenance.* " & _
            "FROM QryTDB LEFT JOIN Maintenance ON QryTDB.Equipment = Maintenance.Equipment " & WhereByStr

    Call replaceStrSQL("QryCountMaint_BaseData", strSQL)
End Sub


Sub QryCountMaint_Insp(Criteria As String)

    strSQL = "SELECT QryCountMaint_BaseData." & Criteria & ", Count(QryCountMaint_BaseData.ID) AS [Inspections Total], " & _
        "(Sum(IIf([Short Descript]='Completed',1,0))) AS [Inspections Completed], Round(Sum(IIf([Short Descript]='Completed',[Reported time],0)),2) AS " & _
        "[Inspections Completed (Booked Time)], Round(Sum(IIf([Short Descript]='Completed',[Planned time],0)),2) AS [Inspections Completed (Planned Time)], " & _
        "IIf([Inspections Completed (Planned Time)]=0,0,Round([Inspections Completed (Booked Time)]/[Inspections Completed (Planned Time)],2)) AS [Inspections Completed Time Utilisation Factor], " & _
        "Sum(IIf([Short Descript]=""Planned"",1,0)) AS [Inspections Remaining], Round(Sum(IIf([Short Descript]='Planned',[Planned time],0)),2) AS [Inspections Remaining (Planned Time)] " & _
        "FROM QryCountMaint_BaseData " & _
        "WHERE (((QryCountMaint_BaseData.[Visit Type])=" & Chr(34) & "I" & Chr(34) & ")) " & _
        "GROUP BY QryCountMaint_BaseData." & Criteria & ";"

    Call replaceStrSQL("QryCountMaint_Insp", strSQL)
End Sub


Sub QryCountMaint_Major(Criteria As String)

    strSQL = "SELECT QryCountMaint_BaseData." & Criteria & ", Count(QryCountMaint_BaseData.ID) AS [Majors Total], " & _
        "(Sum(IIf([Short Descript]='Completed',1,0))) AS [Majors Completed], Round(Sum(IIf([Short Descript]='Completed',[Reported time],0)),2) " & _
        "AS [Majors Completed (Booked Time)], Round(Sum(IIf([Short Descript]='Completed',[Planned time],0)),2) AS [Majors Completed (Planned Time)], " & _
        "IIf([Majors Completed (Planned Time)]=0,0,Round([Majors Completed (Booked Time)]/[Majors Completed (Planned Time)],2)) AS [Majors Completed Time Utilisation Factor], " & _
        "Sum(IIf([Short Descript]=""Planned"",1,0)) AS [Majors Remaining], Round(Sum(IIf([Short Descript]='Planned',[Planned time],0)),2) AS " & _
        "[Majors Remaining (Planned Time)] " & _
        "FROM QryCountMaint_BaseData " & _
        "WHERE (((QryCountMaint_BaseData.[Visit Type])=" & Chr(34) & "T" & Chr(34) & ")) " & _
        "GROUP BY QryCountMaint_BaseData." & Criteria & ";"

    Call replaceStrSQL("QryCountMaint_Major", strSQL)
End Sub


Sub QryCountMaintTime(Criteria As String)
    strSQL = "SELECT QryCountMaint_BaseData." & Criteria & ", QryCountMaint_BaseData.[Visit Type], Sum(QryCountMaint_BaseData.[Reported time]) AS [Booked Time], " & _
                "Sum(QryCountMaint_BaseData.[Planned time]) AS [Planned Time], Round(([Booked Time]/[Planned Time])*100, 0) AS [Utilization Percentage] " & _
                "FROM QryCountMaint_BaseData " & _
                "WHERE (((QryCountMaint_BaseData.[Completion date]) Is Not Null)) " & _
                "GROUP BY QryCountMaint_BaseData." & Criteria & ", QryCountMaint_BaseData.[Visit Type];"


    Call replaceStrSQL("QryCountMaintTime", strSQL)
End Sub





Sub QryCountMaintTime_graph(Criteria As String, order As String)

    Dim firstColumn As String
    Dim fromTable As String
    Dim baseQry As String: baseQry = "QryCountMaintTime"

    ' we want a copy of the value of Criteria. We don't want to edit it within the sub
    Dim localCriteria As String: localCriteria = Criteria

    Call getSQLDependingOnIfTechnicianNumber(localCriteria, baseQry, firstColumn, fromTable)
  
    strSQL = "SELECT TOP 10 " & firstColumn & ", Sum(IIf([Visit Type]=" & Chr(34) & "I" & Chr(34) & ",[Utilization Percentage],0)) AS [Utilization Percentage For I], " & _
                "Sum(IIf([Visit Type]=" & Chr(34) & "T" & Chr(34) & ",[Utilization Percentage],0)) AS [Utilization Percentage For T] " & _
                fromTable & _
                "GROUP BY " & firstColumn

    Call applyCrtieriaSortOnSQL(strSQL, baseQry, localCriteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountMaintTime_graph", strSQL)
End Sub


Sub QryCountMaintCompletion(Criteria As String)

    strSQL = "SELECT QryCountMaint_BaseData." & Criteria & ", QryCountMaint_BaseData.[Visit Type], " & _
                "Count(QryCountMaint_BaseData.[Completion date]) AS [Count Complete], Count(QryCountMaint_BaseData.[Planned date]) AS [Count Planned], " & _
                "IIf([Count Planned]=0,0,Round(([Count Complete]/[Count Planned])*100,0)) AS [Completion Percentage] " & _
                "FROM QryCountMaint_BaseData " & _
                "GROUP BY QryCountMaint_BaseData." & Criteria & ", QryCountMaint_BaseData.[Visit Type] " & _
                "HAVING (((Count(QryCountMaint_BaseData.[Completion date])) Is Not Null));"

    Call replaceStrSQL("QryCountMaintCompletion", strSQL)
End Sub


Sub QryCountMaintCompletion_graph(Criteria As String, order As String)

    strSQL = "SELECT TOP 10 QryCountMaintCompletion." & Criteria & ", Sum(IIf([Visit Type]=" & Chr(34) & "I" & Chr(34) & ",[Completion Percentage],0)) AS [Completion Percentage For I], " & _
                "Sum(IIf([Visit Type]=" & Chr(34) & "T" & Chr(34) & ",[Completion Percentage],0)) AS [Completion Percentage For T] " & _
                "FROM QryCountMaintCompletion " & _
                "GROUP BY QryCountMaintCompletion." & Criteria

    Call applyCrtieriaSortOnSQL(strSQL, "QryCountMaintCompletion", Criteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountMaintCompletion_graph", strSQL)
End Sub


Sub QryCountMaintRepairs(Criteria As String)

    strSQL = "SELECT QryCountMaint_BaseData." & Criteria & ", QryCountMaint_BaseData.[Visit Type], Count(Requests.[Work - Number]) AS [Repair Count] " & _
                "FROM QryCountMaint_BaseData LEFT JOIN Requests ON QryCountMaint_BaseData.[Work Ticket Number] = Requests.[Work - Number] " & _
                "GROUP BY QryCountMaint_BaseData." & Criteria & ", QryCountMaint_BaseData.[Visit Type];"

    Call replaceStrSQL("QryCountMaintRepairs", strSQL)
End Sub


Sub QryCountMaintRepairs_graph(Criteria As String, order As String)
    Dim firstColumn As String
    Dim fromTable As String
    Dim baseQry As String: baseQry = "QryCountMaintRepairs"
    Dim localCriteria As String: localCriteria = Criteria

    Call getSQLDependingOnIfTechnicianNumber(localCriteria, baseQry, firstColumn, fromTable)

    strSQL = "SELECT TOP 10 " & firstColumn & ", Sum(IIf([Visit Type]=" & Chr(34) & "I" & Chr(34) & ",[Repair Count],0)) AS [Repair Count For I], " & _
                "Sum(IIf([Visit Type]=" & Chr(34) & "T" & Chr(34) & ",[Repair Count],0)) AS [Repair Count For T] " & _
                fromTable & _
                "GROUP BY " & firstColumn

    Call applyCrtieriaSortOnSQL(strSQL, baseQry, localCriteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountMaintRepairs_graph", strSQL)
End Sub



' ------------------------------ QryCountRequests: Counting Metrics for Requests ----------------------------------------------


Sub QryCountRequests_BaseData(Criteria As String, keepNotifFilter As Boolean)

    Dim GroupByStr As String
    Dim WhereByStr As String

    ' ------------------------------------------------------------------------------------------------- 

    ' If we are looking at attending techs, then we want the techs from the requests table
    ' Else grab the criteria from QryTDB
    If Criteria = "[Technician number]" Then
        strSQL = "SELECT DISTINCTROW Requests.[Personnel number] AS [Technician number]"
        GroupByStr = "GROUP BY Requests.[Personnel number]"
    Else
        strSQL = "SELECT DISTINCTROW QryTDB." & Criteria
        GroupByStr = "GROUP BY QryTDB." & Criteria
    End If

    ' ------------------------------------------------------------------------------------------------- 

    ' Here we decide whether or not we want to filter using txtBeginDate and txtEndDate
    If keepNotifFilter = True Then
        WhereByStr = "WHERE (((Requests.[FL Quotation Request]) Is Not Null) AND ((Requests.[Creation date])>=[Forms]![frmMTBCMainMenu]![txtBeginDate] And " & _
                        "(Requests.[Creation date])<=[Forms]![frmMTBCMainMenu]![txtEndDate])) "

    ' here we don't want to use the notification filter, and want all the data
    Else
        WhereByStr = "WHERE (((Requests.[FL Quotation Request]) Is Not Null)) "
    End If

    ' ------------------------------------------------------------------------------------------------- 

    ' TO CHANGE THE GROUPING: edit the switch statement
    ' Switch(Condition1, value1 given that condition1 true, condition2, value2, condition3, value3)

    ' Currently it is saying that for   [condition1: Quot Req status = B or D, value1: grouping of A]
    '                                   [condition2: Quot Req status = I or M, value2: grouping of B]
    '                                   [condition3: Quot Req status = P or R, value3: grouping of C]

    strSQL = strSQL & ", Requests.[FL Quotation Request], Requests.[Work - Number], " & _
        "Requests.Identificator, " & _
        "Switch([Identificator]=""CBK"",""CBK"",[Identificator]=""MNT"" Or [Identificator]=""REP"" Or [Identificator]=""PSI"",""MNT, REP, PSI"",True,""other"") AS [Identificator Group], " & _
        "Requests.[FL Quot Req status], " & _
        "Switch([FL Quot Req status]=""B"" Or [FL Quot Req status]=""D"",""A"",[FL Quot Req status]=""I"" Or [FL Quot Req status] " & _
        "=""M"",""B"",[FL Quot Req status]=""P"" Or [FL Quot Req status]=""R"",""C"") AS [Custom Classifier] " & _
        "FROM QryTDB LEFT JOIN Requests ON QryTDB.Equipment = Requests.Equipment " & _
        WhereByStr & GroupByStr & ", Requests.[FL Quotation Request], Requests.[Work - Number], " & _
        "Requests.Identificator, Requests.[FL Quot Req status];"


    Call replaceStrSQL("QryCountRequests_BaseData", strSQL)
End Sub


Sub QryCountRequests_Status(Criteria As String)
    strSQL = "SELECT QryCountRequests_BaseData." & Criteria & ", QryCountRequests_BaseData.[Custom Classifier], " & _
        "Count(QryCountRequests_BaseData.[FL Quotation Request]) AS [Count] " & _
        "FROM QryCountRequests_BaseData " & _
        "GROUP BY QryCountRequests_BaseData." & Criteria & ", QryCountRequests_BaseData.[Custom Classifier];"

    Call replaceStrSQL("QryCountRequests_Status", strSQL)
End Sub


Sub QryCountRequests_Status_graph(Criteria As String, order As String)

    Dim firstColumn As String
    Dim fromTable As String
    Dim baseQry As String: baseQry = "QryCountRequests_Status"
    Dim localCriteria As String: localCriteria = Criteria

    Call getSQLDependingOnIfTechnicianNumber(localCriteria, baseQry, firstColumn, fromTable)

    strSQL = "SELECT TOP 10 " & firstColumn & ", " & _
        "Sum(IIf([Custom Classifier]=" & Chr(34) & "A" & Chr(34) & ",[Count],0)) AS [Count For A], " & _
        "Sum(IIf([Custom Classifier]=" & Chr(34) & "B" & Chr(34) & ",[Count],0)) AS [Count For B], " & _
        "Sum(IIf([Custom Classifier]=" & Chr(34) & "C" & Chr(34) & ",[Count],0)) AS [Count For C] " & _
        fromTable & _
        "GROUP BY " & firstColumn
    
    Call applyCrtieriaSortOnSQL(strSQL, baseQry, localCriteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountRequests_Status_graph", strSQL)
End Sub


Sub QryCountRequests_Status_graph_allResults(Criteria As String, order As String)
    strSQL = "SELECT QryCountRequests_Status." & Criteria & ", Sum(IIf([Custom Classifier]=""A"",[Count],0)) AS [Count For A], " & _
                "Sum(IIf([Custom Classifier]=""B"",[Count],0)) AS [Count For B], Sum(IIf([Custom Classifier]=""C"",[Count],0)) AS [Count For C] " & _
                "FROM QryCountRequests_Status " & _
                "GROUP BY QryCountRequests_Status." & Criteria & ";"

    Call replaceStrSQL("QryCountRequests_Status_graph_allResults", strSQL)
End Sub


Sub QryCountRequests_Origin(Criteria As String)

    strSQL = "SELECT QryCountRequests_BaseData." & Criteria & ", QryCountRequests_BaseData.[Identificator Group], " & _
        "Count(QryCountRequests_BaseData.[FL Quotation Request]) AS [Count Requests] " & _
        "FROM QryCountRequests_BaseData " & _
        "WHERE (((QryCountRequests_BaseData.[Identificator Group])<>""other"")) " & _
        "GROUP BY QryCountRequests_BaseData." & Criteria & ", QryCountRequests_BaseData.[Identificator Group];"

    Call replaceStrSQL("QryCountRequests_Origin", strSQL)
End Sub


Sub QryCountRequests_Origin_graph(Criteria As String, order As String)

    Dim firstColumn As String
    Dim fromTable As String
    Dim baseQry As String: baseQry = "QryCountRequests_Origin"
    Dim localCriteria As String: localCriteria = Criteria

    Call getSQLDependingOnIfTechnicianNumber(localCriteria, baseQry, firstColumn, fromTable)

    strSQL = "SELECT TOP 10 " & firstColumn & ", Sum(IIf([Identificator Group]=" & Chr(34) & "CBK" & Chr(34) & ",[Count Requests],0)) " & _
        "AS [Count CBK], Sum(IIf([Identificator Group]=" & Chr(34) & "MNT, REP, PSI" & Chr(34) & ",[Count Requests],0)) AS [Count MNT, REP, PSI] " & _
        fromTable & _
        "GROUP BY " & firstColumn

    Call applyCrtieriaSortOnSQL(strSQL, baseQry, localCriteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountRequests_Origin_graph", strSQL)
End Sub


'***************************************************************************
' Purpose: This query is identical to the one above, except that we don't
'       only take the top 10 results. This is because we need this data for
'       QryCreateResults_by_x. We need to create two separate queries, though
'       they are similar because we need to use them both at the same time.
' Kinzey
'***************************************************************************
Sub QryCountRequests_Origin_graph_allResults(Criteria As String, order As String)
    strSQL = "SELECT QryCountRequests_Origin." & Criteria & ", Sum(IIf([Identificator Group]=" & Chr(34) & "CBK" & Chr(34) & ",[Count Requests],0)) " & _
        "AS [Count CBK], Sum(IIf([Identificator Group]=" & Chr(34) & "MNT, REP, PSI" & Chr(34) & ",[Count Requests],0)) AS [Count MNT, REP, PSI] " & _
        "FROM QryCountRequests_Origin " & _
        "GROUP BY QryCountRequests_Origin." & Criteria

    Call applyCrtieriaSortOnSQL(strSQL, "QryCountRequests_Origin", Criteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountRequests_Origin_graph_allResults", strSQL)
End Sub


' -----------------------------------------------------------------------------------------------------------------


Sub QryCountMTBC_UniqueEquipment(Criteria As String)
    ' Here we label the first column as "Criteria" this is because the Criteria parameter could be "[Equipment]"
    '       This would cause two columns to be called Equipment. Hence we label the first column as called "Criteria"
    strSQL = "SELECT QryTDB." & Criteria & " AS Criteria, QryTDB.Equipment " & _
                "FROM QryTDB GROUP BY QryTDB." & Criteria & ", QryTDB.Equipment;"

    Call replaceStrSQL("QryCountMTBC_UniqueEquipment", strSQL)
End Sub


Sub QryCountMTBC_UnitsInRange(Criteria As String)

    ' If the Criteria here is Equipment, it works out as it will give each distinct equipment number a value of 1
    strSQL = "SELECT QryCountMTBC_UniqueEquipment.Criteria, Count(QryCountMTBC_UniqueEquipment.Equipment) AS EquipmentCount " & _
                "FROM QryCountMTBC_UniqueEquipment " & _
                "GROUP BY QryCountMTBC_UniqueEquipment.Criteria;"
    
    Call replaceStrSQL("QryCountMTBC_UnitsInRange", strSQL)
End Sub


Sub QryCountMTBC(Criteria As String)

    strSQL = "SELECT QryTDB." & Criteria & ", QryCountMTBC_UnitsInRange.EquipmentCount, " & _
                "(Sum(IIf([SCS A group]='T',1,0))) AS [T Calls], DateDiff(""d"",[Forms]!frmMTBCMainMenu![txtBeginDate],[Forms]!frmMTBCMainMenu![txtEndDate])+1 AS Days, " & _
                "IIf([T Calls]=0,0,Round((([EquipmentCount]*[Days])/[T Calls]),2)) AS MTBC " & _
                "FROM QryCountMTBC_UnitsInRange INNER JOIN QryTDB ON QryCountMTBC_UnitsInRange.Criteria = QryTDB." & Criteria & _
                "WHERE (((QryTDB.[Notification date])>=[Forms]![frmMTBCMainMenu]![txtBeginDate] And (QryTDB.[Notification date])<=[Forms]![frmMTBCMainMenu]![txtEndDate])) " & _
                "GROUP BY QryTDB." & Criteria & ", QryCountMTBC_UnitsInRange.EquipmentCount " & _
                "ORDER BY (Sum(IIf([SCS A group]='T',1,0)));"

    Call replaceStrSQL("QryCountMTBC", strSQL)
End Sub


Sub QryCountMTBC_graph(Criteria As String, order As String)
    strSQL = "SELECT TOP 10 QryCountMTBC." & Criteria & ", Round([MTBC],2) AS MTBC2" & _
                " FROM QryCountMTBC " & _
                " GROUP BY QryCountMTBC." & Criteria & ", Round([MTBC],2)"

    Call applyCrtieriaSortOnSQL(strSQL, "QryCountmtbc", Criteria, order, "Round([MTBC],2)")
    Call replaceStrSQL("QryCountMTBC_graph", strSQL)
End Sub


Sub QryCountSick_Setup(Criteria As String)
    strSQL = "SELECT QryTDB." & Criteria & ", QryTDB.Equipment AS [Equipment Number], QryTDB.[Notification date], QryTDB.[SCS A group] " & _
                "FROM QryTDB " & _
                "GROUP BY QryTDB." & Criteria & ", QryTDB.Equipment, QryTDB.[Notification date], QryTDB.CallID, QryTDB.[SCS A group] " & _
                "HAVING (((QryTDB.[Notification date]) Is Not Null) AND ((QryTDB.[SCS A group])=""T"")) " & _
                "ORDER BY QryTDB." & Criteria & ", QryTDB.Equipment, QryTDB.[Notification date];"

    ' strSQL = "SELECT tblSickUnits_TestInput.* FROM tblSickUnits_TestInput;"

    Call replaceStrSQL("QryCountSick_Setup", strSQL)
End Sub


Sub QryCountSick_PostCalc_Sort(Criteria As String)
    strSQL = "SELECT tblSickUnits." & Criteria & ", tblSickUnits.[Equipment Number], tblSickUnits.[Notification date], " & _
                "tblSickUnits.[Num Of Calls Last 30 Days], tblSickUnits.isSick " & _
                "FROM tblSickUnits " & _
                "ORDER BY tblSickUnits." & Criteria & ", tblSickUnits.[Equipment Number], tblSickUnits.[Notification date];"

    Call replaceStrSQL("QryCountSick_PostCalc_Sort", strSQL)
End Sub


Sub QryCountSick_PostCalc_Results(Criteria As String)
    strSQL = "SELECT QryCountSick_PostCalc_Sort." & Criteria & ", Sum(IIf([isSick]=""SICK_LAST"",1,0)) AS [Sick Units] " & _
                "FROM QryCountSick_PostCalc_Sort " & _
                "GROUP BY QryCountSick_PostCalc_Sort." & Criteria & ";"

    Call replaceStrSQL("QryCountSick_PostCalc_Results", strSQL)
End Sub


Sub QryCountSick_graph(Criteria As String, order As String)
    strSQL = "SELECT TOP 10 QryCountSick_PostCalc_Results." & Criteria & ", QryCountSick_PostCalc_Results.[Sick Units] " & _
                "FROM QryCountSick_PostCalc_Results "

    Call applyCrtieriaSortOnSQL(strSQL, "QryCountSick_PostCalc_Results", Criteria, order, "QryCountSick_PostCalc_Results.[Sick Units]")
    Call replaceStrSQL("QryCountSick_graph", strSQL)
End Sub


' ------------------------------ QryCountRequests: Counting Metrics for Orders ----------------------------------------------


Sub QryCountOrders_BaseData(Criteria As String, keepNotifFilter As Boolean)

    ' This is to add the option of whether you want to do additional date filtering using the txtBeginDate and txtEndDate that was used
    ' for filtering the call notifications
    Dim WhereByStr As String

    If keepNotifFilter = True Then
        WhereByStr = "WHERE (((Repairs.[Job origin Date])>=[Forms]![frmMTBCMainMenu]![txtBeginDate] And (Repairs.[Job origin Date])<=[Forms]![frmMTBCMainMenu]![txtEndDate])) "
    Else
        WhereByStr = ""
    End If

    ' TO CHANGE THE GROUPING: edit the switch statement
    ' Switch(Condition1, value1 given that condition1 true, condition2, value2, condition3, value3)

    ' Currently it is saying that for   [condition1: order status = A or B or D, value1: grouping of A]
    '                                   [condition2: order status = E or F or G, value2: grouping of B]
    ' etc.

    strSQL = "SELECT DISTINCTROW QryTDB." & Criteria & ", Repairs.ID, Repairs.[Repair Order Status], Switch([Repair Order Status]=""A"" Or [Repair Order Status]=""B"" Or " & _
                "[Repair Order Status]=""D"",""A"",[Repair Order Status]=""E"" Or [Repair Order Status]=""F"" Or [Repair Order Status]=""G"",""B"",[Repair Order Status]=""W"" Or " & _
                "[Repair Order Status]=""X"",""C"") AS [Custom Classifier] " & _
                "FROM QryTDB LEFT JOIN Repairs ON QryTDB.Equipment = Repairs.Equipment " & WhereByStr & _
                "GROUP BY QryTDB." & Criteria & ", Repairs.ID, Repairs.[Repair Order Status];"

    Call replaceStrSQL("QryCountOrders_BaseData", strSQL)
End Sub


Sub QryCountOrders_Status(Criteria As String)
    strSQL = "SELECT DISTINCTROW QryCountOrders_BaseData." & Criteria & ", QryCountOrders_BaseData.[Custom Classifier], Count(QryCountOrders_BaseData.ID) AS [Count Orders] " & _
                "FROM QryCountOrders_BaseData " & _
                "WHERE (((QryCountOrders_BaseData.[Custom Classifier]) Is Not Null)) " & _
                "GROUP BY QryCountOrders_BaseData." & Criteria & ", QryCountOrders_BaseData.[Custom Classifier];"

    Call replaceStrSQL("QryCountOrders_Status", strSQL)
End Sub


Sub QryCountOrders_Status_graph(Criteria As String, order As String)
    strSQL = "SELECT TOP 10 QryCountOrders_Status." & Criteria & ", " & _
                "Sum(IIf([Custom Classifier]=""A"",[Count Orders],0)) AS [Count Orders For A], " & _
                "Sum(IIf([Custom Classifier]=""B"",[Count Orders],0)) AS [Count Orders For B], " & _
                "Sum(IIf([Custom Classifier]=""C"",[Count Orders],0)) AS [Count Orders For C] " & _
                "FROM QryCountOrders_Status " & _
                "GROUP BY QryCountOrders_Status." & Criteria

    Call applyCrtieriaSortOnSQL(strSQL, "QryCountOrders_Status", Criteria, order, generateOrder2and3(order))
    Call replaceStrSQL("QryCountOrders_Status_graph", strSQL)
End Sub


Sub QryCountOrders_Status_graph_allResults(Criteria As String, order As String)
    strSQL = "SELECT QryCountOrders_Status." & Criteria & ", " & _
                "Sum(IIf([Custom Classifier]=""A"",[Count Orders],0)) AS [Count Orders For A], " & _
                "Sum(IIf([Custom Classifier]=""B"",[Count Orders],0)) AS [Count Orders For B], " & _
                "Sum(IIf([Custom Classifier]=""C"",[Count Orders],0)) AS [Count Orders For C] " & _
                "FROM QryCountOrders_Status " & _
                "GROUP BY QryCountOrders_Status." & Criteria & ";"

    Call replaceStrSQL("QryCountOrders_Status_graph_allResults", strSQL)
End Sub


' ------------------------------------ Personnel Number To Name Queries ----------------------------------------------------

Sub QryPersonnel_CallDump(Criteria As String)

    If Criteria <> "[Technician number]" Then
        Exit Sub
    End If

    strSQL = "SELECT QryCountNotifications." & Criteria & ", QryTDB.[Tech Last Name] " & _
                "FROM QryTDB RIGHT JOIN QryCountNotifications ON QryTDB." & Criteria & " = QryCountNotifications." & Criteria & " " & _
                "GROUP BY QryCountNotifications." & Criteria & ", QryTDB.[Tech Last Name] " & _
                "ORDER BY QryCountNotifications." & Criteria & ", QryTDB.[Tech Last Name];"

    Call replaceStrSQL("QryPersonnel_CallDump", strSQL)
End Sub


Sub QryPersonnel_Maint(Criteria As String)
    If Criteria <> "[Technician number]" Then
        Exit Sub
    End If

    strSQL = "SELECT QryCountMaint_BaseData.[Personnel number] AS [Technician number], [Last name]+" & Chr(34) & " " & Chr(34) & "+[First name] AS [Maint Name] " & _
                "FROM QryCountMaint_BaseData " & _
                "GROUP BY QryCountMaint_BaseData.[Personnel number], [Last name]+" & Chr(34) & " " & Chr(34) & "+[First name] " & _
                "ORDER BY [Last name]+" & Chr(34) & " " & Chr(34) & "+[First name];"

    Call replaceStrSQL("QryPersonnel_Maint", strSQL)
End Sub


Sub QryPersonnel_Requests(Criteria As String)

    If Criteria <> "[Technician number]" Then
        Exit Sub
    End If

    strSQL = "SELECT QryCountRequests_BaseData." & Criteria & ", QryTDB.[Tech Last Name] " & _
                "FROM QryCountRequests_BaseData LEFT JOIN QryTDB ON QryCountRequests_BaseData." & Criteria & " = QryTDB." & Criteria & "" & _
                "GROUP BY QryCountRequests_BaseData." & Criteria & ", QryTDB.[Tech Last Name];"

    Call replaceStrSQL("QryPersonnel_Requests", strSQL)
End Sub


Sub QryPersonnel_AllNumbers(Criteria As String)
    If Criteria <> "[Technician number]" Then
        Exit Sub
    End If

    strSQL = "SELECT QryCountNotifications." & Criteria & " FROM QryCountNotifications UNION " & _
                "SELECT QryCountMaint_BaseData." & Criteria & " " & _
                "FROM QryCountMaint_BaseData " & _
                "UNION SELECT QryCountRequests_BaseData." & Criteria & " " & _
                "FROM QryCountRequests_BaseData;"

    Call replaceStrSQL("QryPersonnel_AllNumbers", strSQL)
End Sub


Sub QryPersonnel_NumToName(Criteria As String)
    If Criteria <> "[Technician number]" Then
        Exit Sub
    End If

    strSQL = "SELECT QryPersonnel_AllNumbers." & Criteria & " AS [Attending Tech Personnel Number], " & _
                "Switch([QryPersonnel_CallDump].[Tech Last Name] Is Not Null,[QryPersonnel_CallDump].[Tech Last Name], " & _
                "[QryPersonnel_Maint].[Maint Name] Is Not Null,[QryPersonnel_Maint].[Maint Name], " & _
                "[QryPersonnel_Requests].[Tech Last Name] Is Not Null,[QryPersonnel_Requests].[Tech Last Name], " & _
                "True,[QryPersonnel_AllNumbers]." & Criteria & ") AS [Attending Tech Name] " & _
                "INTO tblPersonnelNumToName " & _
                "FROM ((QryPersonnel_AllNumbers LEFT JOIN QryPersonnel_CallDump ON QryPersonnel_AllNumbers." & Criteria & " = " & _
                "QryPersonnel_CallDump." & Criteria & ") LEFT JOIN QryPersonnel_Maint ON QryPersonnel_AllNumbers." & Criteria & " = " & _
                "QryPersonnel_Maint." & Criteria & ") LEFT JOIN QryPersonnel_Requests ON QryPersonnel_AllNumbers." & Criteria & " = QryPersonnel_Requests." & Criteria & ";"

    Call replaceStrSQL("QryPersonnel_NumToName", strSQL)
End Sub


Sub QryCreateResults_by_x(Criteria As String)

    ' ---------------------- This is the next version of the strSQL for QryCreateResults_by_x -----------------------
    '           This is part of the final project requirements by Tom in January 2022
    '           We had to alter it again since the required fields and queries have changed.
    strSQL = "SELECT QryCountMTBC." & Criteria & ", QryCountMTBC.[EquipmentCount] AS Units, " & _
        "QryCountNotifications.T AS [T Calls], QryCountNotifications.E AS [E Calls], Round(QryCountMTBC.[MTBC],1) AS MTBC2, " & _
        "QryCountSick_PostCalc_Results.[Sick Units], IIf([E]=0,0,Round([E]/[T],2)) AS [Ratio (E/T)], " & _
        "QryCountCallDuration.[Calls (norm)] AS [Normal Time Calls], QryCountCallDuration.[Average Minutes (norm)] AS [Av Duration of Calls (Norm)], " & _
        "QryCountShutdowns.[Shutdowns (norm)], QryCountShutdowns.[Back in Serv (norm)], " & _
        "QryCountShutdowns.[Normal Time (%)] AS [Normal Time Shutdown (%)], QryCountCallDuration.[Calls (OT)] AS [After Hours Calls]," & _
        "QryCountCallDuration.[Average Minutes (OT)] AS [Av Duration of Calls (AH)], QryCountShutdowns.[Shutdowns (Aft Hrs)] AS [Shutdowns (AH)], " & _
        "QryCountShutdowns.[Back in Serv (Aft Hrs)] AS [Back in Serv (AH)], QryCountShutdowns.[After Hours (%)] AS [After Hours Shutdown (%)], " & _
        "QryCountMaint_Insp.[Inspections Total], QryCountMaint_Insp.[Inspections Completed], QryCountMaint_Insp.[Inspections Completed (Booked Time)], " & _
        "QryCountMaint_Insp.[Inspections Completed (Planned Time)], QryCountMaint_Insp.[Inspections Completed Time Utilisation Factor], QryCountMaint_Insp.[Inspections Remaining], QryCountMaint_Insp.[Inspections Remaining (Planned Time)], " & _
        "QryCountMaint_Major.[Majors Total], QryCountMaint_Major.[Majors Completed], QryCountMaint_Major.[Majors Completed (Booked Time)], " & _
        "QryCountMaint_Major.[Majors Completed (Planned Time)], QryCountMaint_Major.[Majors Completed Time Utilisation Factor], QryCountMaint_Major.[Majors Remaining], QryCountMaint_Major.[Majors Remaining (Planned Time)], " & _
        "QryCountRequests_Origin_graph_allResults.[Count CBK] AS [Repair Requests (Origin CBK)], QryCountRequests_Origin_graph_allResults.[Count MNT, REP, PSI] AS [Repair Requests (Origin MNT, REP, PSI)], " & _
        "QryCountRequests_Status_graph_allResults.[Count For A] AS [Repair Requests (Status Group 1)], QryCountRequests_Status_graph_allResults.[Count For B] AS [Repair Requests (Status Group 2)], QryCountRequests_Status_graph_allResults.[Count For C] AS [Repair Requests (Status Group 3)], " & _
        "QryCountOrders_Status_graph_allResults.[Count Orders For A] AS [Repair Orders (Status Group 1)], QryCountOrders_Status_graph_allResults.[Count Orders For B] AS [Repair Orders (Status Group 2)], QryCountOrders_Status_graph_allResults.[Count Orders For C] AS [Repair Orders (Status Group 3)] " & _
        "INTO Results_by_x FROM ((QryCountMaint_Major INNER JOIN (QryCountMaint_Insp INNER JOIN (QryCountRequests_Origin_graph_allResults INNER JOIN " & _
        "(QryCountSick_PostCalc_Results INNER JOIN (((QryCountMTBC INNER JOIN QryCountCallDuration ON QryCountMTBC." & Criteria & " = " & _
        "QryCountCallDuration." & Criteria & ") INNER JOIN QryCountNotifications ON QryCountMTBC." & Criteria & " = " & _
        "QryCountNotifications." & Criteria & ") INNER JOIN QryCountShutdowns ON QryCountMTBC." & Criteria & " = " & _
        "QryCountShutdowns." & Criteria & ") ON QryCountSick_PostCalc_Results." & Criteria & " = " & _
        "QryCountMTBC." & Criteria & ") ON QryCountRequests_Origin_graph_allResults." & Criteria & " = QryCountSick_PostCalc_Results." & Criteria & ") " & _
        "ON QryCountMaint_Insp." & Criteria & " = QryCountSick_PostCalc_Results." & Criteria & ") ON QryCountMaint_Major." & Criteria & " = " & _
        "QryCountMaint_Insp." & Criteria & ") INNER JOIN QryCountRequests_Status_graph_allResults ON QryCountMTBC." & Criteria & " = " & _
        "QryCountRequests_Status_graph_allResults." & Criteria & ") INNER JOIN QryCountOrders_Status_graph_allResults ON QryCountMTBC." & Criteria & " = " & _
        "QryCountOrders_Status_graph_allResults." & Criteria & ";"

    Call replaceStrSQL("QryCreateResults_by_x", strSQL)

End Sub



Sub QryCreateResults_by_x_IndividualTech(Criteria As String)

    ' Note the first column does not depend on Critieria, but will always be [Attending Tech Name]
    ' Here I also didn't use Criteria, since it isn't necessary. The criteria will always be the same.
    strSQL = "SELECT tblPersonnelNumToName.[Attending Tech Name], QryCountNotifications.T AS [T Calls], QryCountNotifications.E AS [E Calls], " & _
        "IIf([E]=0,0,Round([E]/[T],1)) AS [Ratio (E/T)], QryCountCallDuration.[Calls (norm)] AS [Normal Time Calls], " & _
        "QryCountCallDuration.[Average Minutes (norm)] AS [Av Duration of Calls (Norm)], QryCountShutdowns.[Shutdowns (norm)], " & _
        "QryCountShutdowns.[Back in Serv (norm)], QryCountShutdowns.[Normal Time (%)] AS [Normal Time Shutdown (%)], " & _
        "QryCountCallDuration.[Calls (OT)] AS [After Hours Calls], QryCountCallDuration.[Average Minutes (OT)] AS [Av Duration of Calls (AH)], " & _
        "QryCountShutdowns.[Shutdowns (Aft Hrs)] AS [Shutdowns (AH)], QryCountShutdowns.[Back in Serv (Aft Hrs)] AS [Back in Serv (AH)], " & _
        "QryCountShutdowns.[After Hours (%)] AS [After Hours Shutdown (%)], QryCountMaint_Insp.[Inspections Completed], " & _
        "QryCountMaint_Insp.[Inspections Completed (Booked Time)], QryCountMaint_Insp.[Inspections Completed (Planned Time)], " & _
        "QryCountMaint_Insp.[Inspections Completed Time Utilisation Factor], QryCountFirstTimeFix_graph_all.YesCount AS [First Time Fix Counter (Yes)], " & _
        "QryCountFirstTimeFix_graph_all.NoCount AS [First Time Fix Counter (No)], QryCountMaint_Major.[Majors Completed], " & _
        "QryCountMaint_Major.[Majors Completed (Booked Time)], QryCountMaint_Major.[Majors Completed (Planned Time)], " & _
        "QryCountMaint_Major.[Majors Completed Time Utilisation Factor], QryCountRequests_Origin_graph_allResults.[Count CBK] AS [Repair Requests (Origin CBK)], " & _
        "QryCountRequests_Origin_graph_allResults.[Count MNT, REP, PSI] AS [Repair Requests (Origin MNT PSI REP)], " & _
        "QryCountRequests_Status_graph_allResults.[Count For A] AS [Repair requests (status group 1)], " & _
        "QryCountRequests_Status_graph_allResults.[Count For B] AS [Repair requests (status group 2)], " & _
        "QryCountRequests_Status_graph_allResults.[Count For C] AS [Repair requests (status group 3)] INTO Results_by_x FROM (((((((tblPersonnelNumToName " & _
        "LEFT JOIN QryCountFirstTimeFix_graph_all ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountFirstTimeFix_graph_all.[Technician number]) " & _
        "LEFT JOIN QryCountNotifications ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountNotifications.[Technician number]) " & _
        "LEFT JOIN QryCountRequests_Origin_graph_allResults ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountRequests_Origin_graph_allResults.[Technician number]) " & _
        "LEFT JOIN QryCountMaint_Major ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountMaint_Major.[Technician number]) " & _
        "LEFT JOIN QryCountMaint_Insp ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountMaint_Insp.[Technician number]) " & _
        "LEFT JOIN QryCountShutdowns ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountShutdowns.[Technician number]) " & _
        "LEFT JOIN QryCountCallDuration ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountCallDuration.[Technician number]) " & _
        "LEFT JOIN QryCountRequests_Status_graph_allResults ON tblPersonnelNumToName.[Attending Tech Personnel Number] = QryCountRequests_Status_graph_allResults.[Technician number] " & _
        "ORDER BY tblPersonnelNumToName.[Attending Tech Name];"

    Call replaceStrSQL("QryCreateResults_by_x_IndividualTech", strSQL)
End Sub


' ------------------------------------------------------------------------------------------------------------------------------


Sub QryRequestsSummary_OrderRequests(equipStr As String)
    ' Generate the strSQL to replace the existing one
    strSQL = "SELECT QryRequestsRepairs_FullOuterJoin.[Creation date] AS [Request Creation], " & _
        "QryRequestsRepairs_FullOuterJoin.[Rep Job description] AS [Request Text], QryRequestsRepairs_FullOuterJoin.[Material Description], " & _
        "QryRequestsRepairs_FullOuterJoin.[Changed on] AS [Request Change], QryRequestsRepairs_FullOuterJoin.[FL Quot Req status] AS [Request Status], " & _
        "QryRequestsRepairs_FullOuterJoin.Repairs.[Sales Document] AS [Order Number], QryRequestsRepairs_FullOuterJoin.Repairs.[Short Text] AS " & _
        "[Order text], QryRequestsRepairs_FullOuterJoin.Repairs.[Repair Order Status] AS [Order Status] FROM QryRequestsRepairs_FullOuterJoin " & _
        "WHERE (((QryRequestsRepairs_FullOuterJoin.Requests.Equipment) = " & Chr(34) & equipStr & Chr(34) & ")) Or (((QryRequestsRepairs_FullOuterJoin.Repairs.Equipment) = " & Chr(34) & equipStr & Chr(34) & ")) " & _
        "ORDER BY QryRequestsRepairs_FullOuterJoin.[Creation date] DESC;"

    ' now we replace the existing strSQL with our modified strSQL
    Call replaceStrSQL("QryRequestsSummary_Order&Requests", strSQL)
End Sub
