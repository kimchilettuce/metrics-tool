Option Compare Database
Option Explicit

Private Sub btnResetDates_Click()

    Dim defaultStr As String
    defaultStr = "Import Call Dump File Needed"

    ' This grabs the largest/smallest date in Call Dump
    txtBeginDate.Value = DMin("[Notification date]", "Call Dump")
    txtEndDate.Value = DMax("[Notification date]", "Call Dump")

    ' this means Call Dump file is empty
    If IsNull(txtBeginDate.Value) Then
        txtBeginDate.Value = defaultStr
        txtEndDate.Value = defaultStr
    End If

End Sub

'***************************************************************************
' Purpose: Sets the values of txtBeginDate and txtEndDate based on entries
' in Call Dump.
' Kinzey
'***************************************************************************
Public Sub Form_Load()
    Call btnResetDates_Click
    
    ' clear any filter choices
    lblSLWorkCenters.Caption = ""
    lblFilterManufacturer.Caption = ""
    lblFilterProducts.Caption = ""
    lblFilterContractType.Caption = ""
    lblFilterTechnician.Caption = ""

    Call verionControlFrontEnd(Application.CurrentProject.Name)
End Sub


'***************************************************************************
' Purpose: Informs the user when the current version of this frontend being
'           used is outdated and that there is a later version. Note, the
'           developer needs to update the value of the latest version stored
'           as a value in the tbl [Frontend Version Control]
' Kinzey
'***************************************************************************
Private Sub verionControlFrontEnd(fileName As String)
    Dim splitFileName() As String
    Dim versionNumber As String
    Dim dbLatestVersion As String

    ' Removing the .accdb at the end of the fileName
    fileName = Left(fileName, Len(fileName) - 6)
    splitFileName = Split(fileName, " ")
    
    ' this should store the version number in splitFileName(2)
    '       given that the fileName is of the format:
    '       Reliability tool v8.3.1.aacdb
    
    versionNumber = splitFileName(2)
    
    If versionNumber Like "v?.?.?" Then
        dbLatestVersion = DLookup("[Latest Frontend Version]", "[Frontend Version Control]")

        If compareSemVer(versionNumber, dbLatestVersion) < 0 Then
            MsgBox "You are currently using frontend user version " & versionNumber & ", however there is frontend user version " & dbLatestVersion & _
                    " located in filepath S:\Aus_NI\NI Business\Tom White\Reliability Metrics Tool. It is recommended to use this more updated version.", vbExclamation, _
                    "Outdated App Version"
        End If
    End If
End Sub


' ---------- Buttons Clicked ---------

Private Sub btnFilterContractType_Click()
    Update_QryTDB
    DoCmd.OpenForm "frmFilterContractType"
End Sub

Private Sub btnFilterManufacturer_Click()
    Update_QryTDB
    DoCmd.OpenForm "frmFilterManufacturer"
End Sub

Private Sub btnFilterProducts_Click()
    Update_QryTDB
    DoCmd.OpenForm "frmFilterProducts"
End Sub

Private Sub btnFilterRegions_Click()
    Update_QryTDB
    DoCmd.OpenForm "frmFilterSLWorkCenters"
End Sub

Private Sub btnFilterTechnician_Click()
    Update_QryTDB
    DoCmd.OpenForm "frmFilterTechnician"
End Sub


'***************************************************************************
' Purpose: Used to open the form to import files.
' Kinzey
'***************************************************************************
Private Sub btnImportData_Click()
    Screen.ActiveForm.Visible = False
    DoCmd.OpenForm "frmImportData"
End Sub


'***************************************************************************
' Purpose: Used to load and move into the main overview form
' Tom White
'***************************************************************************
Private Sub btnNextForm_Click()

    Dim summaryMsg As String
    
    ' Update the stringss
    Call Update_QryTDB
    
    ' Display the filter summary selection by msgbox, and also add it to the
    '       tblSettings for the other form to use later.
    summaryMsg = genStrFilterSummary()
    MsgBox summaryMsg, vbInformation, "Summary of Filters Selected"
    txtSummaryFilter.Value = summaryMsg

    ' use the line below, instead of using (DoCmd.Minimize)
    Screen.ActiveForm.Visible = False
    
    ' ---------------------------- Logic Calculating Sick units --------------------------------
    Dim Criteria As String: Criteria = "[Service Leader WC]"

    Call QryTDB(txtStrFilter.Value)

    Call QryCountSick_Setup(Criteria)
    Call DoRecordByRecord_logic(Criteria, "QryCountSick_Setup", "tblSickUnits")
    ' -------------------------------------------------------------------------------------------

    'Open the Form which calls the query
    DoCmd.OpenForm "frmQryTDB_Result"
End Sub


' --------------------------------------------------------------


'***************************************************************************
' Purpose: Used to open the update the SQL string in qryTDB. Used to create
'           a dynamic SQL query result
' Tom
'***************************************************************************
Private Sub Update_QryTDB()
    On Error GoTo ERR_HANDLE
    Dim MyDB As DAO.Database
    Dim qdef As DAO.QueryDef
    Dim strSQL As String
    Dim strFilter As String: strFilter = ""
    
    Dim strSLWCs, strManufacturer, strProds, strContractType, strTechnician, strDates As String
    
    Set MyDB = CurrentDb()
       
    ' Close existing query to refresh the result
    ' Kinz: I think is regarding to how, if a query is currently open, the changes made to the strSQL won't be saved
    '           it will need to have its tab closed first. The line below can be tested later if it is necessary or helpful.
    'DoCmd.Close acQuery, "QryTDB"
    

    ' ------------------ Query defintion for QryTDB ----------------------------------

    ' Deprecated:
    '       - no longer want to add filtering of dates here, since we have it already done QryCallDumpApplyDateFilter
    '       - this is because adding the date filter here got rid of blanks in notification.date which were important
    '0) Filter Dates
    ' If Not txtBeginDate.Value = "" And Not txtEndDate.Value = "" Then

    '     ' Kinzey: this seems to be working on the main overview screen, and most places, including the MTBC stats
    '     '           however, it seems to be causing a problem in loading pie and line charts in the individual equipment overview forms
    '     '           something about an OLE server overwhelmed.
    '     '           If this filtering of dates is not rly wanted, we can leave it out for now.
    '     GoSub Filter_Dates
    ' End If

    '1) Filter SL Work Centers
        strSLWCs = lblSLWorkCenters.Caption
        
        If Not Trim(strSLWCs) = "" Then
            GoSub Filter_SLWorkCenters
        End If
            
    '2) Filter Manufacturer
        strManufacturer = lblFilterManufacturer.Caption
        
        If Not Trim(strManufacturer) = "" Then
            GoSub Filter_Manufacturer
        End If
        
    '3) Filter Products
        strProds = lblFilterProducts.Caption
        
        If Not Trim(strProds) = "" Then
            GoSub Filter_ProductLines
        End If
        
    '4) Filter Contract Types
        strContractType = lblFilterContractType.Caption
        
        If Not Trim(strContractType) = "" Then
            GoSub Filter_ContractType
        End If
        
    '5) Filter Technician
        strTechnician = lblFilterTechnician.Caption
        
        If Not Trim(strTechnician) = "" Then
            GoSub Filter_Technician
        End If

    ' --------------------------------------------------------

    ' applying last ordering
    strFilter = strFilter & ") ORDER BY [Call Dump].[Notification date] DESC , UNITS.TDB.Equipment;"

    txtStrFilter.Value = strFilter

    ' this is used to update the SQL for QryTDB and takes in a single parameter that contains the
    '       SQL string to apply additional filtering
    ' Please take a look at the helper module called "UpdateStrSQL.bas"
    Call QryTDB(strFilter)

    ' Exit out of sub here
Exit_cmdOpenQuery_Click:
    Exit Sub
    
    
    '------------------ Error Handling --------------
ERR_HANDLE:

    If Err.Number = 5 Then
        MsgBox "You must make a selection(s) from the list" _
               , , "Selection Required !"
        Resume Exit_cmdOpenQuery_Click
    Else
        'Write out the error and exit the sub
        MsgBox Err.Description
        Resume Exit_cmdOpenQuery_Click
    End If


    '----------------------------------------------------
    '-------------- strSQL Modifiers --------------------
    '----------------------------------------------------

    '------------------ Filter Dates ----------------
Filter_Dates:

    ' this line was here previously
    ' strDates = "(([Call Dump].[Notification date] is null))"

    ' filter based on begin and end dates
    strDates = " AND ([Call Dump].[Notification date])>=[Forms]![frmMTBCMainMenu]![txtBeginDate] AND ([Call Dump].[Notification date])<=[Forms]![frmMTBCMainMenu]![txtEndDate]"
    strFilter = strFilter & strDates

    Return
        
    '----------- Filter SL Work Center --------------
Filter_SLWorkCenters:
        
    strSLWCs = " AND [Service Leader WC] in " & _
    "(" & Left(strSLWCs, Len(strSLWCs) - 1) & ")"
    
    strFilter = strFilter & strSLWCs
        
    Return

    '--------- Filter Product Line -----------------
Filter_ProductLines:

    strProds = " AND [Product Lines NI/MOD/ESC] in " & _
    "(" & Left(strProds, Len(strProds) - 1) & ")"

    strFilter = strFilter & strProds
        
    Return

    '--------- Filter Manufacturer -----------------
Filter_Manufacturer:

    strManufacturer = " AND [Equipment Manufacturer] in " & _
    "(" & Left(strManufacturer, Len(strManufacturer) - 1) & ")"

    strFilter = strFilter & strManufacturer
    
    Return

    '-------- Filter Contract Type ------------------
Filter_ContractType:

    strContractType = " AND [Contract Type] in " & _
    "(" & Left(strContractType, Len(strContractType) - 1) & ")"

    strFilter = strFilter & strContractType
    
    Return

    '------------------ Filter Technician -----------
Filter_Technician:

    strTechnician = " AND [Service Run] in " & _
    "(" & Left(strTechnician, Len(strTechnician) - 1) & ")"

    strFilter = strFilter & strTechnician
    
    Return

End Sub


'***************************************************************************
' Purpose: Used to generate a string that summarises the filers chosen. This
'           string will also be displayed on frmQryTDB_Result
' Kinzey
'***************************************************************************
Public Function genStrFilterSummary() As String

    Dim msg As String


    msg = "Filter Notification By: {Start date: " & txtBeginDate.Value & ", End date: " & txtEndDate.Value & "}" & vbNewLine & _
    "Filters: " & vbNewLine & _
    "      Service Area: " & lblSLWorkCenters.Caption & vbNewLine & _
    "      Manufacturer: " & lblFilterManufacturer.Caption & vbNewLine & _
    "      Products: " & lblFilterProducts.Caption & vbNewLine & _
    "      Contract Type: " & lblFilterContractType.Caption & vbNewLine & _
    "      Run: " & lblFilterTechnician.Caption & vbNewLine & _
    "Note: Blank filters indicate no filter was chosen."

    msg = Replace(msg, "'", "")
    genStrFilterSummary = msg
End Function


'***************************************************************************
' Purpose: Used to return a MsgBox for what filters are applied to the next
' form frmQryTDB_Result
' [Deprecated]
' Tom
'***************************************************************************
Private Sub CheckInputs()
    Dim strMSG As String: strMSG = ""
    
    '0) Filter Dates
        If txtBeginDate.Value = "" Or txtEndDate.Value = "" Then
            strMSG = strMSG & "No DATE filter applied"
            Exit Sub
        End If

    '1) Filter SL Work Centers
        If lblSLWorkCenters.Caption = "" Then
            strMSG = strMSG & vbCrLf & "No SL WORK CENTER filter applied"
        End If
        
    '2) Filter Manufacturer
        If lblFilterManufacturer.Caption = "" Then
            strMSG = strMSG & vbCrLf & "No MANUFACTURER filter applied"
        End If
             
    '3) Filter Products
        If lblFilterProducts.Caption = "" Then
            strMSG = strMSG & vbCrLf & "No PRODUCT filter applied"
        End If
    
        
    '4) Filter Contract Types
        If lblFilterContractType.Caption = "" Then
            strMSG = strMSG & vbCrLf & "No CONTRACT TYPE filter applied"
        End If
        
    '5) Filter Technician
        If lblFilterTechnician.Caption = "" Then
            strMSG = strMSG & vbCrLf & "No RUN filter applied"
        End If
        
    MsgBox strMSG

End Sub

' ----------------------------------------------------------------------------------------------------

'***************************************************************************
' Purpose: These two subs below are used to validate whether the dates chosen
'           are valid as begin and ending dates.
' Kinzey
'***************************************************************************
Private Sub txtBeginDate_Change()
    ' Debug.Print txtBeginDate.Text
    ' Debug.Print txtEndDate.Value
    
    ' This function is a bit weird. It seems it returns the number of days
    '       of (EndDate - BeginDate). So when the BeginDate is ahead of the EndDate
    '       it returns a negative value.
    If DateDiff("d", txtBeginDate.Text, txtEndDate.Value) < 0 Then
        MsgBox "The begining date you've entered is past the end date. Begining date is reset to default, please try again.", vbExclamation
        txtBeginDate.Value = DMin("[Notification date]", "Call Dump")
    End If
End Sub



Private Sub txtEndDate_Change()
    If DateDiff("d", txtBeginDate.Value, txtEndDate.Text) < 0 Then
        MsgBox "The ending date you've entered is before the begining date. Ending date is reset to default, please try again.", vbExclamation
        txtEndDate.Value = DMax("[Notification date]", "Call Dump")
    End If
End Sub
