Private Sub btnFindRecord_Click()

    Dim strEquipmentRef As String
    Dim strEquipmentStart As String
    Dim strSearch As String
    
    'Check txtSearch for Null value or Nill Entry first.

    If IsNull(Me![txtSearch]) Or (Me![txtSearch]) = "" Then
        MsgBox "Please enter a value!", vbOKOnly, "Invalid Search Criterion!"
        Me![txtSearch].SetFocus
        Exit Sub
    End If
    
    Equipment.SetFocus
    strEquipmentStart = Equipment.Text

    DoCmd.ShowAllRecords
    DoCmd.GoToControl ("Equipment")
    DoCmd.FindRecord Me!txtSearch
    
    Equipment.SetFocus
    strEquipmentRef = Equipment.Text
    txtSearch.SetFocus
    strSearch = txtSearch.Text
        
    'If matching record found sets focus in strStudentID and shows msgbox
    'and clears search control
    If strEquipmentRef = strSearch Then
        MsgBox "Match Found For: " & strSearch, , "Equipment Found!"
        Equipment.SetFocus
        txtSearch = ""
        
    'If value not found sets focus back to txtSearch and shows msgbox
    Else
        MsgBox "Match Not Found For: " & strSearch & " - Please Try Again.", _
        , "Invalid Search Criterion!"
        
        DoCmd.ShowAllRecords
        DoCmd.GoToControl ("Equipment")
        DoCmd.FindRecord strEquipmentStart
        
        txtSearch.SetFocus
    End If
End Sub


Private Sub Command104_Click()
    DoCmd.Close
    Forms!frmQryTDB_Result.Visible = True
End Sub


Private Sub Form_Load()
    DoCmd.Maximize
    
End Sub



