Option Compare Database
Public graphcriteria As String
Public graphorder As String ' graph order

Dim highLow As Boolean
Dim strSQL As String

' what query is currently being displayed on the graph
' Used in update_graph to find how many entries the graph is trying to display
Dim currentQry As String

Dim ListByOptions As Variant
Dim ListByMetrics As Variant

' FTF: First Time Fix, default value
Dim defaultTFT_dayDiff As Integer


'***************************************************************************
' Purpose: Can't declare values outside of a sub. Hence we declare global variables
'           here and call this sub at form load.
' Kinzey
'***************************************************************************
Private Sub initGlobalValues()
    ListByOptions = Array("Equipment", "Product", "SL", "Run", "Rated Speed", "Rated Load", "Num Stops", "Attending Technician")
    ListByMetrics = Array( _
                        "Notifications", _
                        "MTBC", _
                        "Shutdowns", _
                        "Call Duration", _
                        "Sick Units", _
                        "Maint. Completion", _
                        "Maint. Repairs", _
                        "Maint. Time", _
                        "Request Status", _
                        "Order Status", _
                        "Request Origin", _
                        "First Time Fix" _
                    )
    

    ' Change the values here to change the default values of the first time fix
    defaultTFT_dayDiff = 147
End Sub


'***************************************************************************
' Purpose: The row source takes its value list to be elements separated by a
'           ";". And so this function takes in an array and converts it to the
'           form: "elem1;elem2;elem3".
' Kinzey
'***************************************************************************
Private Function formatToRowSource(arrayList As Variant) As String
    Dim elem As Variant
    
    formatToRowSource = ""
    For Each elem In arrayList
        formatToRowSource = formatToRowSource & elem & ";"
    Next elem
End Function


'***************************************************************************
' Purpose: This dynamically creates a description of what is being graphed
'           in the label called lblGraphDescription. You can edit this sub
'           to add more description based on the metric and option chosen.
' Kinzey
'***************************************************************************
Private Sub generateGraphDescription()
    Dim addtionalText As String: addtionalText = ""
    Dim strMetric As String: strMetric = comboBoxMetric.Value
    Dim strOption As String: strOption = comboBoxByOption.Value

    ' You can use this to specify what some abbreviations are
    If strOption = "SL" Then
        strOption = "Service Leader (SL)"
    End If

    ' You can add cases for what you wan the additional text to be
    If strMetric = "Shutdowns" Then
        addtionalText = " [testing that additional Text works for shutdowns]"
    End If

    ' Adding nice brackets around the string
    strMetric = "[" & strMetric & "]"
    strOption = "[" & strOption & "]"

    Me.lblGraphDescription.Caption = "The graph is showing " & strMetric & " by " & strOption & addtionalText
End Sub


'***************************************************************************
' Purpose: This handles the event where the option of Graph Metric is changed,
'           by selecting a new option on the combo box.
' Kinzey
'***************************************************************************
Private Sub comboBoxMetric_Change()
    
    ' want ListByOptions to reset back to its default value
    ' Since in some cases, elements of the list will be removed
    Call initGlobalValues
    
    Dim newRowSourceQry As String
    Dim newChartTitle As String

    
    ' remove spaces and make text lowercase so less likely to make spelling mistakes
    Select Case Trim(Replace(LCase(comboBoxMetric.Text), " ", ""))

        Case "notifications":
            newRowSourceQry = "QryCountNotifications_graph"
            newChartTitle = "COUNT NOTIFICATIONS"


        Case "mtbc":
            newRowSourceQry = "QryCountMTBC_graph"
            newChartTitle = "ESTIMATE MTBC"

            ' Remove the last option "Attending technician"
            ReDim Preserve ListByOptions(UBound(ListByOptions) - 1)


        Case "shutdowns":
            newRowSourceQry = "QryCountShutdowns_graph"
            newChartTitle = "CALLS RESULTING IN SHUTDOWN (%)"
        

        Case "callduration":
            newRowSourceQry = "QryCountCallDuration_graph"
            newChartTitle = "AVERAGE CALL DURATION"


        Case "sickunits":
            newRowSourceQry = "QryCountSick_graph"
            newChartTitle = "COUNT OF SICK UNITS"

            ' Remove the last option "Attending technician"
            ReDim Preserve ListByOptions(UBound(ListByOptions) - 1)
        
        Case "firsttimefix":
            newRowSourceQry = "QryCountFirstTimeFix_graph"
            newChartTitle = "COUNT OF FIRST TIME FIX"

            ' only has the option of attending technician
            ListByOptions = Array("Attending Technician")

        Case "maint.completion":
            newRowSourceQry = "QryCountMaintCompletion_graph"
            newChartTitle = "PERCENTAGE OF COMPLETE MAINT."
            ReDim Preserve ListByOptions(UBound(ListByOptions) - 1)


        Case "maint.time":
            newRowSourceQry = "QryCountMaintTime_graph"
            newChartTitle = "UTILIZATION PERCENT. OF BOOKED TIME"


        Case "maint.repairs":
            newRowSourceQry = "QryCountMaintRepairs_graph"
            newChartTitle = "NUM OF REPAIRS RAISED"


        Case "requeststatus":
            newRowSourceQry = "QryCountRequests_Status_graph"
            newChartTitle = "NUM REQUESTS BASED ON STATUS"


        Case "orderstatus":
            newRowSourceQry = "QryCountOrders_Status_graph"
            newChartTitle = "NUM ORDERS BASED ON STATUS"


        Case "requestorigin":
            newRowSourceQry = "QryCountRequests_Origin_graph"
            newChartTitle = "NUM REQUESTS BASED ON ORIGIN"


        Case Else:
            MsgBox "Invalid error, comboBox value not registered"

    End Select

    Call generateGraphDescription

    ' store this global variable
    currentQry = newRowSourceQry

    ' Update the query the graph is running on, note this will automatically update the graph
    Graph.RowSource = newRowSourceQry
    Graph.ChartTitle.Text = newChartTitle

    ' update the options avaliable for by options
    comboBoxByOption.RowSource = formatToRowSource(ListByOptions)
End Sub



'***************************************************************************
' Purpose: This combobox allows the user to select what object to compare by.
'           This sub does the branching of what happens when a particular option
'           is selected.
' Kinzey
'***************************************************************************
Private Sub comboBoxByOption_Change()

    ' reset values of the default lists
    Call initGlobalValues
    
    Dim newGraphCriteria As String

    ' remove spaces and make text lowercase so less likely to make spelling mistakes
    Select Case Trim(Replace(LCase(comboBoxByOption.Text), " ", ""))
        Case "ratedload":
            newGraphCriteria = "[Rated Load]"
            
            ' remove first time fix as an option
            ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)


        Case "product":
            newGraphCriteria = "[Product Lines NI/MOD/ESC]"
            ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)


        Case "ratedspeed":
            newGraphCriteria = "[Rated Speed]"
            ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)


        Case "sl":
            newGraphCriteria = "[Service Leader WC]"
            ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)


        Case "numstops":
            newGraphCriteria = "[Num of Stops]"
            ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)


        Case "run":
            newGraphCriteria = "[Service Run]"
            ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)

        
        Case "equipment":
            newGraphCriteria = "[Equipment]"
            ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)


        Case "attendingtechnician":
            newGraphCriteria = "[Technician number]"
            ListByMetrics = Array("Notifications", "Shutdowns", "Call Duration", "First Time Fix", "Maint. Time", "Maint. Repairs", "Request Status", "Request Origin")


        Case Else:
            MsgBox "Invalid error, comboBox value not registered"

    End Select

    Call generateGraphDescription
    Call Update_Graph(newGraphCriteria, graphorder)
   
    ' update the metrics avaliable, based on what by option picked
    comboBoxMetric.RowSource = formatToRowSource(ListByMetrics)
End Sub


' ------------------------------------------------ Other Buttons -----------------------------------------------

'***************************************************************************
' Purpose: Clicking back button to go back to start menu.
' Tom
'***************************************************************************
Private Sub btnGoBack_Click()
    DoCmd.Close acForm, "frmQryTDB_Result", acSaveNo
    DoCmd.OpenForm "frmMTBCMainMenu"
End Sub


'***************************************************************************
' Purpose: Used to open a form that can set the settings for First Time Fix.
' Kinzey
'***************************************************************************
Private Sub btnSettings_Click()
    DoCmd.OpenForm "frmQryTDB_Settings"
End Sub


'***************************************************************************
' Purpose: Displays a message box so the user can view the full summary message.
' Kinzey
'***************************************************************************
Private Sub btnDisplaySummary_Click()
    MsgBox Forms!frmMTBCMainMenu!txtSummaryFilter, vbInformation, "Summary of Filters Selected"
End Sub


'***************************************************************************
' Purpose: Clicking the "low-high"/"high-low" button to change the ordering
'           of the graph.
' Tom
'***************************************************************************
Private Sub btnSortHighToLow_Click()
    highLow = Not highLow
    
    ' just clicked on highlow
    If highLow = False Then
        Call Update_Graph(graphcriteria, "lowhigh")
        btnSortHighToLow.Caption = "High To Low"
    Else
        Call Update_Graph(graphcriteria, "highlow")
        btnSortHighToLow.Caption = "Low To High"
    End If
End Sub


'***************************************************************************
' Purpose: Clicking "alphabetical button to sort the graph by alphabetical
'           order.
' Tom
'***************************************************************************
Private Sub btnSortAlphabetical_Click()
    Call Update_Graph(graphcriteria, "alphabetical")
End Sub


'***************************************************************************
' Purpose: Used to export the results to an excel file for further use of
'           pivot tables and fine analysis that is unable to be done
'           efffectively on Access. Note that the queries run here directly
'           create tables.
' Kinzey
'***************************************************************************
Private Sub btnExport_Click()
    DoCmd.Hourglass True
    
    ' with two types of export, we will have two separate files to export to.
    Dim WorkbookName As String
    Dim excelMacro As String

    ' ---------------------------------------------------------------

    ' When running the query, a warning will appear if you are sure to
    '       write into the table. We want to ignore this warning.
    Call DoCmd.SetWarnings(False)

    ' [Tech last name] is the same as "Tech[Call]" and they will have a separate type of export
    If graphcriteria = "[Technician number]" Then
        WorkbookName = "ExportReport_IndividualTech"
        excelMacro = ""

        ' OpenQuery will run the query which will directly produce the results in the table alrdy selected
        Call QryCreateResults_by_x_IndividualTech(graphcriteria)
        DoCmd.OpenQuery "QryCreateResults_by_x_IndividualTech"

    ' Everything else will have the main export
    Else
        ' Dynamically create results_by_x SQL
        WorkbookName = "ExportReport_Main"
        excelMacro = ""

        ' This is an old example of how you can automatically run a macro at the export
        'excelMacro = "shStaticDataInput.getDataInput"

        Call QryCreateResults_by_x(graphcriteria)
        DoCmd.OpenQuery "QryCreateResults_by_x"
    End If
        
    ' This is a function in another module in this accb file. Check out the modules on the left hand side.
    Call ExportResults.exportResultByX(WorkbookName, excelMacro)
    
    ' ---------------------------------------------------------------

    ' Don't want the user to click a bunch of buttons as warnings are raised. We know what we are doing.
    Call DoCmd.SetWarnings(True)
    Call DoCmd.Hourglass(False)
    
End Sub


' ------------------------------------------------------------------------------------------------------------



'***************************************************************************
' Purpose: Used when double click on a particular unit in the bottom splitview
' Kinzey
'***************************************************************************
Private Sub Form_DblClick(Cancel As Integer)

    ' ------------- Dynamically Generate RequestsRepairs strSQL ----------------------
    ' When the single unit snapshot loads, we want to dynmically generate the SQL string for requests
    ' this is because the SQL is a bit complex, requiring a full outer join of tblRequests and tblRepairs
    ' to get all possible combinations of requests and repairs.
    '
    ' Note: We want to place the code here at the double click, to generate the SQL before the next subform is loaded
    '       that way, at the subform load, it will run this fresh new strSQL.

    ' Step 1: get the equipment number of the equipment we are getting snapshot of
    
    ' Dim equipNum As Double: equipNum = Forms![frmSummary_TDBSingleUnit]![Equipment].Value
    Dim equipNum As Double: equipNum = Me.Equipment
    Dim equipStr As String: equipStr = Trim(CStr(equipNum))

    ' Step 2: We apply this particular equipment number formatted as a string to modify the SQL
    Call QryRequestsSummary_OrderRequests(equipStr)
    ' ----------------------------------------------------------------------------------
    
    ' Note that QryTDB_Units has a ID field
    recordID = Me.ID
    Screen.ActiveForm.Visible = False

    ' Opens the form but also adds a particular criteria where the "ID = " & recordID
    ' this ensures that the form is regarding the particular unit you double clicked.
    DoCmd.OpenForm "frmSummary_TDBSingleUnit", , , "ID = " & recordID
End Sub


'***************************************************************************
' Purpose: Used when double click on a particular unit in the bottom splitview
' Kinzey
'***************************************************************************
Private Sub Form_Load()
    
    ' Initialising values
    Dim loadingString As String: loadingString = "~Loading~"
    Dim onSiteOvertime As String
    
    lblDays.Caption = loadingString
    lblUnits.Caption = loadingString
    lblCalls.Caption = loadingString
    lblMTBC.Caption = loadingString
    
    lblshutdowns.Caption = loadingString
    lblVisitDurationNorm.Caption = loadingString
    lblVisitDurationOT.Caption = loadingString
    lblSick.Caption = loadingString

    lblGraphDescription.Caption = loadingString


    ' Initialise the global variables that we need access to.
    Call initGlobalValues

    ' ------------------------------------------------------------------------------

    ' Want to clear the old results of personnel number to personnel name
    ' this will allow it to be generated freshly
    DoCmd.SetWarnings False
    DoCmd.RunSQL ("DELETE * FROM tblPersonnelNumToName")
    DoCmd.SetWarnings True
    
    '------------ Set the default query to be QryCountNotifcations -----------------

     ' Set default sorting of the graph ot be Low to High
    highLow = True
    btnSortHighToLow.Caption = "Low To High"

    ' Set global variable that the first query is counting notifications
    currentQry = "QryCountNotifications_graph"

    ' setting the default graph to be sorted by SL
    Call Update_Graph("[Service Leader WC]", "highlow", True)

    ' setting the graph to first be displaying notifications.
    Graph.RowSource = "QryCountNotifications_graph"
    Graph.ChartTitle.Text = "COUNT NOTIFICATIONS"

    ' Correctly setup the options for graphing
    comboBoxByOption.RowSource = formatToRowSource(ListByOptions)

    ' Correctly setup the metrics for graphing
    ReDim Preserve ListByMetrics(UBound(ListByMetrics) - 1)
    comboBoxMetric.RowSource = formatToRowSource(ListByMetrics)


    ' Set the default string for the label describing what the graph is doing
    Me.lblGraphDescription.Caption = "The graph is showing " & Me.comboBoxMetric.Value & " by " & Me.comboBoxByOption.Value

    ' ------------------------------------------------------------------------------

    ' Display the filter summary on the label
    lblFilterCriteria.Caption = Forms!frmMTBCMainMenu!txtSummaryFilter

    ' this setting is used in the label displays below
    onSiteOvertime = getSettings("Call Back: On-site Outlier (mins)")

    'comment
    ' ^ wow thanks Tom for such a helpful comment
    ' ^ TW 080322: No, thank YOU
    ' TW 101322: The [minutes] <= 600 filtering for normal / OT here is different to the 480 in QryCountCallDuration and the 240 requested by Dan
    lblDays.Caption = DateDiff("d", [Forms]!frmMTBCMainMenu![txtBeginDate], [Forms]!frmMTBCMainMenu![txtEndDate]) + 1
    lblUnits.Caption = DSum("[EquipmentCount]", "[QryCountMTBC_UnitsInRange]")
    lblCalls.Caption = DSum("[T]", "[QryCountNotifications]")
    lblMTBC.Caption = Round((lblUnits.Caption * lblDays.Caption) / (lblCalls.Caption), 2)

    lblshutdowns.Caption = (DSum("[Shutdowns (norm)]", "[qrycountshutdowns]") + DSum("[Shutdowns (Aft Hrs)]", "[qrycountshutdowns]"))
    lblshutdowns.Caption = lblshutdowns.Caption & " (" & Round((lblshutdowns.Caption / lblCalls.Caption) * 100, 1) & "%)"

    lblVisitDurationNorm.Caption = Round(DSum("[Minutes]", "[QryTDB]", "[Overtime] = 'Normal' AND [Minutes] <= " & onSiteOvertime) _
                                    / DCount("[notification]", "[QryTDB]", "[Overtime] = 'Normal' AND [Minutes] <= " & onSiteOvertime), 0) & " mins"

    lblVisitDurationOT.Caption = Round(DSum("[Minutes]", "[QryTDB]", "[Overtime] = 'OT' AND [Minutes] <= " & onSiteOvertime) _ 
                                    / DCount("[notification]", "[QryTDB]", "[Overtime] = 'OT' AND [Minutes] <= " & onSiteOvertime), 0) & " mins"
    
    ' Note: this works because the starting the starting option is "Service Leader"
    lblSick.Caption = DSum("[Sick Units]", "[QryCountSick_PostCalc_Results]")
    
    ' TW 080322
    ' commented out this next maximize to control formatting of graph/form
    'DoCmd.Maximize
    
End Sub


'***************************************************************************
' Purpose: Notice that Update_Graph is used for when you want to specifically
'           update the criteria, the "per", for what you are graphing. Also
'           used for setting the order of what is called.
' Tom
'***************************************************************************
Sub Update_Graph(Criteria As String, order As String, Optional onFormLoad As Boolean = False)
    
    Call DoCmd.Hourglass(True)

    ' Call the Subs that will dynamically change the strSQL contained in each
    '       SQL definition based on the criteria and order

    ' Dynamically update the SQL str for QryTDB
    Call QryTDB(Forms!frmMTBCMainMenu!txtStrFilter)
    ' ----------------------- First Time Fix ---------------------------

    ' These are the subs for dealing with First Time Fix
    Call QryCountFirstTimeFix(Criteria)

    If Criteria = "[Technician number]" Then
        Call DoRecordByRecord_logic(Criteria, "QryCountFirstTimeFix", "tblFirstTimeFix")
    End If

    Call QryCountFirstTimeFix_graph(Criteria, order)
    Call QryCountFirstTimeFix_graph_all(Criteria, order)
    
    ' -------------------------------------------------------------------

    Call QryCreateResults_by_x(Criteria)
    Call QryCreateResults_by_x_IndividualTech(Criteria)

    ' these two queries are used in the setup for calculating QryCountMTBC
    Call QryCountMTBC_UniqueEquipment(Criteria)
    Call QryCountMTBC_UnitsInRange(Criteria)

    Call QryCountMTBC(Criteria)
    Call QryCountMTBC_graph(Criteria, order)

    Call QryCountNotifications(Criteria)
    Call QryCountNotifications_Graph(Criteria, order)

    Call QryCountMaint_Insp(Criteria)
    Call QryCountMaint_Major(Criteria)
    Call QryCountShutdowns(Criteria)
    Call QryCountCallDuration(Criteria)
    
    Call QryCountShutdowns_Graph(Criteria, order)
    Call QryCountCallDuration_graph(Criteria, order)
    
    ' ------ Sick Units --------
    Call QryCountSick_Setup(Criteria)

    ' If onFormLoad = True, we don't want to do the SickUnits logic since it would cause a record lock
    '       error with the tblSickUnits already opened by frmQryTDB_Result.
    '       Hence, we separated the first sick unit calculation to happen in the Main Menu when the go 
    '       button is pressed. 
    ' Otherwise, if not onFormLoad then can do the logic as usual.
    If onFormLoad = False Then
        Call DoRecordByRecord_logic(Criteria, "QryCountSick_Setup", "tblSickUnits")
    End If

    Call QryCountSick_PostCalc_Sort(Criteria)
    Call QryCountSick_PostCalc_Results(Criteria)
    Call QryCountSick_graph(Criteria, order)

    ' ------ Maintenance -------
    Dim useNotifFilter As Boolean

    useNotifFilter = convert01ToBoolean(DLookup("[User Selection]", "tblSettings", "[Setting Type] = 'Maint Stats Use Notif Dates'"))
    Call QryCountMaint_BaseData(Criteria, useNotifFilter)

    Call QryCountMaintTime(Criteria)
    Call QryCountMaintTime_graph(Criteria, order)

    Call QryCountMaintCompletion(Criteria)
    Call QryCountMaintCompletion_graph(Criteria, order)

    Call QryCountMaintRepairs(Criteria)
    Call QryCountMaintRepairs_graph(Criteria, order)


    ' -------- Requests --------

    useNotifFilter = convert01ToBoolean(DLookup("[User Selection]", "tblSettings", "[Setting Type] = 'Req Stats Use Notif Dates'"))
    Call QryCountRequests_BaseData(Criteria, useNotifFilter)

    Call QryCountRequests_Origin(Criteria)
    Call QryCountRequests_Origin_graph(Criteria, order)
    Call QryCountRequests_Origin_graph_allResults(Criteria, order)

    Call QryCountRequests_Status(Criteria)
    Call QryCountRequests_Status_graph(Criteria, order)
    Call QryCountRequests_Status_graph_allResults(Criteria, order)

    ' -------- Orders ----------

    useNotifFilter = convert01ToBoolean(DLookup("[User Selection]", "tblSettings", "[Setting Type] = 'Orders Stats Use Notif Dates'"))
    Call QryCountOrders_BaseData(Criteria, useNotifFilter)

    Call QryCountOrders_Status(Criteria)
    Call QryCountOrders_Status_graph(Criteria, order)
    Call QryCountOrders_Status_graph_allResults(Criteria, order)

    ' ------ Personnel ---------

    Call QryPersonnel_CallDump(Criteria)
    Call QryPersonnel_Maint(Criteria)
    Call QryPersonnel_Requests(Criteria)
    Call QryPersonnel_AllNumbers(Criteria)
    Call QryPersonnel_NumToName(Criteria)

    ' --------------------------

    ' ----------------------- Generate List of Personnel Number to Personnel Name ---------------------------------

    ' When the form is loaded, tblPersonnelNumToName is emptied
    ' Here we check if the Criteria is Attending Technician, and we check if the table is empty
    '       If it is, we generate the list of number to name again.
    '       Note this will only need to be generated once, and so the table is only emptied once, which was when
    '       the form was loaded.
    If Criteria = "[Technician number]" And DCount("*", "tblPersonnelNumToName") = 0 Then
        Call DoCmd.SetWarnings(False)

        ' This line will output the results to tblPersonnelNumToName
        DoCmd.OpenQuery "QryPersonnel_NumToName"
        Call DoCmd.SetWarnings(True)
    End If

    ' -------------------------------------------------------------------------------------------------------------

    Graph.Requery

    ' ------------------------------------------------
    
    Dim numRows As Integer

    ' numRows is the count of number of entires to appear on the graph.
    '   currentQry is a global variable, it is a string containing the name of the current Qry
    '   being displayed on the graph.
    numRows = DCount("*", currentQry)
    
    ' -------------------------------------------------
    ' TW 080322
    ' commented out this section to control formatting of graph/
    ' instead of dynamically changing height of graph depending on number of rows.. i will try to make a static form.
    
    'DoCmd.Maximize
    
    'If numRows >= 10 Then
        'Graph.Height = 7000
   ' Else
        'Graph.Height = 5000
        'Form.SplitFormSize = 7000
   'End If

    ' -------------------------------------------
    
    ' Store the variables of Criteria and order in global variables
    graphcriteria = Criteria
    graphorder = order

    Call DoCmd.Hourglass(False)
End Sub

' ------------------------------------------------------------------------------------------------------------








