Option Compare Database
Option Explicit

' Notes:
'   later when we want to work with pivot table
'   we can have a hidden table that reads from datainput
'   this is because the columns of datainput dynamically changes
'   hence the pivot tables can read from a table that has static columns
'           make sure the pivot table reads to the last line

'***************************************************************************
' Purpose: This is the main sub that runs the export process. It opens the
'       excel workbook  and copies the data from the table.
'
' Arguments:
'       - WorkbookName  (String) : The name of the workbook you want to export to
'       - excelMacro    (String) : The name of a macro that exists alrdy inside the
'                                   excel workbook. This is run at the end of
'                                   export process. Could be used to run the setup
'                                   and refresh of pivot tables.
'
' Kinzey
'***************************************************************************
Sub exportResultByX(WorkbookName As String, excelMacro As String)
    Dim objXL As excel.Application
    Dim objWkb As excel.Workbook
    Dim objSht As excel.worksheet
    
    Dim db As Database
    Dim rs As Recordset
    Dim intLastCol As Integer
    
    Const conMAX_ROWS = 20000
    Const conSHT_NAME = "DataInput"
    Dim conWKB_NAME As String: conWKB_NAME = "\\Auswsr0014\shared\Aus_NI\NI Business\Tom White\" + _
    "Reliability Metrics Tool\Export Files\" & WorkbookName & ".xlsm"
    
    Set db = CurrentDb
    Set objXL = New excel.Application
    Set rs = db.OpenRecordset("Results_by_x", dbOpenSnapshot)
    
    With objXL
        .Visible = True
        Set objWkb = .Workbooks.Open(conWKB_NAME)
        On Error Resume Next
        Set objSht = objWkb.Worksheets(conSHT_NAME)
        
        If Not Err.Number = 0 Then
          Set objSht = objWkb.Worksheets.Add
          objSht.Name = conSHT_NAME
        End If
        
        Err.Clear
        
        ' disables error handling for the rest of the sub
        On Error GoTo 0
        
        intLastCol = objSht.UsedRange.Columns.Count
        
        ' This sub adds the dynamic setting headers to the main data output sht
        Call setSummaryMsgs(WorkbookName, objSht, True)

        ' ------------- Copy Contents of QryCreate_Results_By_x ---------------

        With objSht
            .Columns("A:A").ColumnWidth = 27
            .Range("A1").WrapText = True

            ' delete existing content first
            .Range(.Cells(3, 1), .Cells(conMAX_ROWS, intLastCol)).ClearContents
            
            ' Set headers to bold
            .Range(.Cells(2, 1), .Cells(2, rs.Fields.Count)).Font.Bold = True
            
            Call copyDataWithHeaders(objSht, rs, .Range("A2"))
        End With
        
        ' ----------------- Print Out Settings to Excel --------------------------

        Set objSht = objWkb.Worksheets("Settings")
        Set rs = db.OpenRecordset("tblSettings", dbOpenSnapshot)

        ' Here we are copying the contents of tblSettings to the page "Settings" in the excel export file
        ' We are then also including the Summary Filter message that is stored in frmMTBCMainMenu
        With objSht
            Call copyDataWithHeaders(objSht, rs, .Range("C2"))
            .Range("A2").Value = Forms!frmMTBCMainMenu!txtSummaryFilter
        End With

        ' ------------------------------------------------------------------------

        ' the user can pass an arugment, to run a macro that is stored in the excel workbook
        If excelMacro <> "" Then
            .Run excelMacro
        End If

        .WindowState = xlMaximized
    End With
    
    Call objWkb.Save
        
    Set objSht = Nothing
    Set objWkb = Nothing
    Set objXL = Nothing
    Set rs = Nothing
    Set db = Nothing
End Sub

'***************************************************************************
' Purpose: Used to copy the recordset into the worksheet but also includes
' the headers
' Kinzey
'***************************************************************************
Sub copyDataWithHeaders(ws As excel.worksheet, rsData As Recordset, startingCell As Range)
    
    Dim rngHeader As Range
    Dim fld As Variant
    
    With ws
        Set rngHeader = startingCell
    
        For Each fld In rsData.Fields
            rngHeader.Value = fld.Name
            Set rngHeader = rngHeader.Offset(, 1)
        Next fld

        startingCell.Offset(1, 0).CopyFromRecordset rsData
    End With
    
End Sub


'***************************************************************************
' Purpose: Its a bit complicated to bolden and underline parts of a cell's contents
'           Hence, this sub is used to help for this issue. It makes a distinction
'           between the title and the body contents. It will bolden and underline
'           the title.
' Kinzey
'***************************************************************************
Sub setMsgToCell(cell As Range, title As String, body As String)

    Dim lenTitle As Integer
    
    ' Intially make sure the cell has bold and underline turned off
    With cell
        .Font.Bold = False
        .Font.Underline = xlUnderlineStyleNone
    End With
    
    ' Add the contents in
    cell.Value = title & vbNewLine & body
    
    ' Calculate the length of the title, so that we can make the title
    '       bold and underlined.
    lenTitle = Len(title)
    With cell.Characters(0, lenTitle)
        .Font.Bold = True
        .Font.Underline = xlUnderlineStyleSingle
    End With
End Sub


'***************************************************************************
' Purpose: Used to fill in the various summary messages at the first row of
'           the export.
' Parameters:
'           worksheet           (excel.Worksheet)   : the worksheet of the export
'                                                       that we are modifying
'
'           exportingForMain    (Boolean)           : If true, it means we
'                                                       are working on the main
'                                                       export, else we are working
'                                                       on the export for individual technician
' Kinzey
'***************************************************************************
Private Sub setSummaryMsgs(WorkbookName As String, worksheet As excel.worksheet, exportingForMain As Boolean)
    ' Read from tblSettings and set the label to show the default value for day diff
    Dim firstFixDays As Integer: firstFixDays = getSettings("FTF Day Diff")
    Dim firstFixIncludeE As Boolean: firstFixIncludeE = convert01ToBoolean(getSettings("FTF Include E Calls"))

    Dim maintFilterUsingNotif As Boolean: maintFilterUsingNotif = convert01ToBoolean(getSettings("Maint Stats Use Notif Dates"))
    Dim reqFilterUsingNotif As Boolean: reqFilterUsingNotif = convert01ToBoolean(getSettings("Req Stats Use Notif Dates"))
    Dim orderFilterUsingNotif As Boolean: orderFilterUsingNotif = convert01ToBoolean(getSettings("Orders Stats Use Notif Dates"))

    Dim onSiteOutlier As String: onSiteOutlier = getSettings("Call Back: On-site Outlier (mins)")
    Dim normalArrivalTime As String: normalArrivalTime = getSettings("Call Back: Normal Arrival Time")
    Dim normalLeaveTime As String: normalLeaveTime = getSettings("Call Back: Normal Leave Time")

    ' TimeSerial is used to add one minute TimeSerial(hour, minutes, seconds)
    Dim afterHoursArrivalTime As String: afterHoursArrivalTime = Format(CStr(TimeValue(normalLeaveTime) + TimeSerial(0, 1, 0)), "hh:mm")
    Dim afterHoursLeaveTime As String: afterHoursLeaveTime = Format(CStr(TimeValue(normalArrivalTime) + TimeSerial(0, -1, 0)), "hh:mm")

    ' -----------------------------------------------

    ' The two differing exports have their summaries messages in different cells
    '       But the logic below remains the same
    Dim exportCells As Variant
    
    If WorkbookName = "ExportReport_IndividualTech" Then
        exportCells = Array("B1", "E1", "J1")
    Else
        exportCells = Array("B1", "H1", "M1")
    End If


    ' This is the message for general reliability
    Call setMsgToCell( _
        worksheet.Range(exportCells(0)), _
        "General Reliability", _
        "Notification Range: " & Forms!frmMTBCMainMenu!txtBeginDate & " to " & Forms!frmMTBCMainMenu!txtEndDate _
    )

    ' This is the message for Callback Handling (normal time)
    Call setMsgToCell( _
        worksheet.Range(exportCells(1)), _
        "Callback Handling (Normal Time)", _
        "SCSA = T only" & vbNewLine & vbNewLine & _
        "On-site outlier <= " & onSiteOutlier & " mins" & vbNewLine & vbNewLine & _
        "Arrival Day: Monday to Friday, AND Arrival Time: " & normalArrivalTime & " to " & normalLeaveTime & vbNewLine & vbNewLine & _
        "Notification Range: " & Forms!frmMTBCMainMenu!txtBeginDate & " to " & Forms!frmMTBCMainMenu!txtEndDate _
    )


    ' This is the message for Callback Handling (after hours)
    Call setMsgToCell( _
        worksheet.Range(exportCells(2)), _
        "Callback Handling (After Hours)", _
        "SCSA = T only" & vbNewLine & vbNewLine & _
        "On-site outlier <= " & onSiteOutlier & " mins" & vbNewLine & vbNewLine & _
        "Arrival Day: Saturday to Sunday, OR Arrival Time: " & afterHoursArrivalTime & " to " & afterHoursLeaveTime & vbNewLine & vbNewLine & _
        "Notification Range: " & Forms!frmMTBCMainMenu!txtBeginDate & " to " & Forms!frmMTBCMainMenu!txtEndDate _
    )
End Sub



